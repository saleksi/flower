<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta charset="utf-8">
</head>
<body>
<?php
	//////////////////////////////////////////////////////////////////
	// klasa za slanje email poruka

	require 'email/PHPMailerAutoload.php';
	$mail = new PHPMailer();

	// postavke kriptiranog slanja email poruke
	$mail->IsSMTP(true);
	$mail->Host = "drava.etfos.hr";
	$mail->SMTPAuth = true;
	$mail->Username = '********************';           // insert Username / email
	$mail->Password = '********************';           // insert Password

	$mail->From="ssamardzija@etfos.hr";
	$mail->FromName="Zalijevanje cvijeća";
	$mail->Sender="ssamardzija@etfos.hr";

	$mail->Subject = "Podsjetnik zalijevanja cvijeća " . date("j.n.Y.");

	$mail->IsHTML(true);
	$mail->CharSet="utf-8";
	$mail->AddEmbeddedImage('images/logo.jpg', 'logo','images/logo.jpg'); // logo
	$mail->Body = "<h3 align='center'>Podsjetnik</h3>
		<p align='center'>Vaše cvijeće treba zaliti</p>
		<p align='center'><a href='http://www.etfos.unios.hr/~ssamardzija/zalijevanje.php'>
			<img src='cid:logo'/></a><br></p>
		<p align='center'>Zahvaljujemo Vam na povjerenju<br>
			<br><a href='http://www.etfos.unios.hr/~ssamardzija'>Zalijevanje Cvijeća</a>.</p>";

	//////////////////////////////////////////////////////////////////

	// povezivanje s bazom
	include "database.php";

	// petlja za svakog korisnika
	$q = "SELECT * FROM korisnici";
	$rUsers = mysqli_query($con, $q); 		// query result for users
	$nUsers = mysqli_num_rows($rUsers);		// number of users
	//echo "<p>$nUsers</p>";
	while($nUsers--)
	{
		$brCvijeca = 0; 										// broj cvijeća za zalijevanje
		$user = mysqli_fetch_row($rUsers);	// dohvati slijedećeg korisnika
		//echo "<p>{$user[0]},{$user[1]},{$user[2]},{$user[3]},{$user[4]},{$user[5]}</p>";

		// petlja za cvijeće korisnika
		$q="CALL DohvatiCvijece({$user[0]})";
		$rFlowers = mysqli_query($con, $q);			// query result for flowers
		$nFlowers = mysqli_num_rows($rFlowers);	// number of user's flowers
		//echo "<p>$nFlowers</p>";
  	while($nFlowers--)
		{
			$flower = mysqli_fetch_row($rFlowers); // dohvati slijedeći cvijet

			$p1=$flower[3]; // period ljetni
			$p2=$flower[4]; // period zimski
			$d1=$flower[5]; // datum zaliven

			list($d,$m,$y)=explode('.',$d1); // rastavi 5.10.2016. na brojeve 5, 10 i 2016
			$zaliven = mktime(0, 0, 0,      $m  ,      $d  ,      $y  ); // datum zaliven
			$danas   = mktime(0, 0, 0, date("m"), date("d"), date("Y")); // datum današnji

			$d = floor(($danas-$zaliven)/86400); // razlika datuma u danima

			if($d>=0){
				$month = date("m");
				$d = ($month>3 && $month<10) ? $d-$p1 : $d-$p2; // ljetni(4,5,6,7,8,9) : zimski(10,11,12,1,2,3)
				if($d>=0)	$brCvijeca++; // broj cvijeća za zalijevanje
			}
		}
		echo "<p>{$user[4]} {$user[5]} treba zaliti $brCvijeca cvijeća</p>";
		if($brCvijeca>0) // ako ima cvijeća za zalijevanje
		$mail->AddBCC($user[3],"{$user[4]} {$user[5]}");	// dodaj email adresu na popis za slanje podsjetnika
	}
	// pošalji podsjetnike
	if(!$mail->Send()) echo "<h3>Pogreška pri slanju: " . $mail->ErrorInfo . "</h3>";
	else echo "<h3>Podsjetnici su poslani</h3>";
?>
</body>
</html>
