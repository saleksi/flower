# Internet aplikacija za evidenciju zalijevanja cvijeća

* [Dokumentacija](https://zir.nsk.hr/islandora/object/etfos:874/datastream/PDF/download)
* [Internet stranica](https://www.etfos.unios.hr/~ssamardzija)
#

### Aplikacija ima slijedeće karakteristika

* Unos, promjena i brisanje podataka u bazi podataka korisnika.
* Pretraživanje po dva kriterija odjednom: naziv cvijeta i barkod cvijeta.
* Forme za unos i promjenu podataka imaju odgovarajuće kontrole na unos podataka koje se odvijaju na klijentu.
* Aplikacija ne dozvoljava unos i promjenu neodgovarajućih podataka.
#

### Upute za pokretanje aplikacije na localhost računalu

* Instalirati **XAMPP** i pokrenuti **MySQL server**.
* Kopirati datoteke u mapu **/XAMPP/htdocs/**.
* Otvoriti internet preglednik, upisati adresu **localhost/phpmyadmin** te stvoriti novu bazu s nazivom **ssamardzija**, colation **utf8\_general\_ci**.
* Unutar phpmyadmin sučelja otvoriti SQL prozor i izvršiti program iz datoteke **/XAMPP/htdocs/sql/ssamardzija.sql**.
* Pokrenuti aplikaciju upisivanjem adrese **localhost** u internet pregledniku.
