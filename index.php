<?php

	// template:  http://cssr.ru/simpliste/#fresh

	include 'database.php'; // spajanje s bazom

	include "main.php"; // sesija, login, submitt form...

	$active = array("class='active'", "", ""); // odabir akritnog polja u meniju

	include "top.php" // html head, main manu, scripts...

?>

<!-- POČETNA STRANICA -->
<script>
	<!-- uređuje se tekst unutar tagova u top.php datoteci -->
	document.getElementById('LocalTitle').innerHTML = "Internet stranica za evidenciju zalijevanja cvijeća";
	document.getElementById('LocalSubTitle').innerHTML = "Na ovoj stranici možete koristiti uslugu evidencije zalijevanja Vašeg cvijeća.";
</script>

<div class="col_33">
  <h2>Upute za uporabu stranice</h2>
  <ul>
	 <li>Registrirajte se za prvo korištenje</li>
	 <li>Prijavite se kao postojeći korisnik</li>
	 <li>Unesite podatke o Vašem cvijeću</li>
	 <li>Isprintajte barkod oznake cvijeća</li>
	 <li>Instalirajte android barkod čitač</li>
	 <li>Zalijte cvijeće kada primite mail</li>
	 <li>Koristite barkod čitač za potvrdu</li>
  </ul>
</div>

<div class="col_33">
  <?php
  if($bLoggedIn==false){ // forma za logiranje korisnika
		echo "<h2>Prijava postojećih korisnika</h2>";
		echo "<form method='post'>
				<p><input required='true' type='text' placeholder='Koriničko ime' tabindex=".++$ti." name='user' autocorrect='off' autocapitalize='none' autocomplete='off' value=''/><br></p>
				<p><input required='true' type='password' placeholder='Lozinka' tabindex=".++$ti." name='pass' autocorrect='off' autocapitalize='none' autocomplete='off' value=''/><br></p>
				<p><input class='button' type='submit' value='Prijava' tabindex=".++$ti." name='login'/><br></p>
				</form>";
  }else{ // forma za odjavu korisnika
		echo "<h2>Dobro došli " . $_SESSION['Ime'] . " " . $_SESSION['Prezime'] . " </h2>";

		$q="CALL DohvatiCvijece({$_SESSION['id']});";
		$res = mysqli_query($con, $q);
		$nC = mysqli_num_rows($res);

		echo "<p>Do sada ste upisali podatke za " . $nC;
			if($nC==1){echo " cvijet";}else if($nC>1 && $nC<5){echo " cvijeta";}else{echo " cvijeća";}
			echo ".</p>";

		echo "<p>U gornjem izborniku odaberite ponuđene mogućnosti.</p>
				<p>U desnom stupcu možete urediti Vaše korisničke podatke.</p>
				<p>Na dolje navedenom linku se možete odjaviti sa stranice.</p>
				<form name='form4' method='post'>
				<p><input class='button' type='submit' value='Odjava korisnika' tabindex=".++$ti." name='logout'/></p>
				</form>";
  } ?>
</div>

<div class="col_33">
  <?php // forma za registraciju korisnika
	if($bLoggedIn==false){
		echo "<h2>Registracija novih korisnika</h2>
			  <form name='form1' method='post' autocomplete='on'>
			  <p><input required='true' type='text' placeholder='Ime' tabindex=".++$ti." name='Ime' /></p>
			  <p><input required='true' type='text' placeholder='Prezime' tabindex=".++$ti." name='Prezime' /></p>
			  <p><input required='true' type='email' placeholder='E-mail' tabindex=".++$ti." name='email'  autocorrect='off' autocapitalize='none'/></p>
			  <p><input required='true' type='text' placeholder='Korisničko ime' tabindex=".++$ti." name='userReg' value='' autocorrect='off' autocapitalize='none'/></p>
			  <p><input required='true' type='text' placeholder='Lozinka'  tabindex=".++$ti." name='passReg' value='' autocorrect='off' autocapitalize='none'/></p>
			  <p><input class='button' type='submit' value='Registracija' tabindex=".++$ti." name='register'/></p>
			  </form>";
	}
	else{ // forma za uređivanje i brisanje korisnika
		echo "<h2>Vaši korisnički podaci</h2>
		<form name='form1' method='post' autocomplete='on'>
		<p><input required='true' type='text'  tabindex=".++$ti." name='Ime' value='". $_SESSION['Ime'] ."'/></p>
		<p><input required='true' type='text'  tabindex=".++$ti." name='Prezime' value='".$_SESSION['Prezime']."'/></p>
		<p><input required='true' type='email' tabindex=".++$ti." name='email' value='".$_SESSION['email']."'/></p>
		<p><input required='true' type='text'  tabindex=".++$ti." name='user' value='".$_SESSION['user']."'/></p>
		<p><input required='true' type='password'  tabindex=".++$ti." name='pass' value='".$_SESSION['pass']."'/></p>
		<p><input class='button' type='submit' value='Spremi' tabindex=".++$ti." name='edit' style='display:inline-block;'
							onclick=\"return confirm('Sigurno želite promijeniti podatke?');\">
		   <input class='button' type='submit' value='Obriši' tabindex=".++$ti." name='delete' style='display:inline-block;margin-left:25px;'
			        onclick=\"return confirm('Sigurno želite obrisati korisnika?');\"></p>
		</form>";
	}
  ?>
</div>

<script>
<?php // uporaba notify.js skripte
		if($_SESSION['KorisnikRegistriran']){
			echo "\$.notify('Novi korisnik uspješno registriran',{className:'success',position:'top left'});";
		}
		if($_SESSION['KorisnikNijeRegistriran']){
			echo "\$.notify('Nije uspjelo zbog postojećeg korisnika s tim imenom i lozinkom',{className:'error',position:'top left'});";
		}
		if($_SESSION['KorisnikIzmijenjen']){
			echo "\$.notify('Korisnički podaci uspješno izmijenjeni',{className:'success',position:'top left'});";
		}
		if($_SESSION['KorisnikNijeIzmijenjen']){
			echo "\$.notify('Nije uspjelo zbog postojećeg korisnika s tim imenom i lozinkom',{className:'error',position:'top left'});";
		}
	?>
</script>

<!-- Footer -->
<?php include "bottom.php"; ?>
