﻿<?php
	//error_reporting(0);

	session_start();

	$ti = 0; // tab indeks (za navigaciju tastaturom)

	// Sortiranje
	$orderByValueArray = array(0=>"", 1=>"nazivu", 2=>"barkodu");
	$orderByKeyArray = array(0=>"", 1=>"ime", 2=>"id");

	// logiranje korisnika
	$bLoggedIn = false;
	if(isset($_SESSION['login']) && $_SESSION['login']==true){
		$bLoggedIn = true;
	}
	elseif(isset($_POST['login'])){
		$q="SELECT * FROM korisnici WHERE(user='".md5($_POST['user'])."' and pass='".md5($_POST['pass'])."')";
		$res = mysqli_query($con, $q);
		if(mysqli_num_rows($res)==1){
			$bLoggedIn = true;
			$_SESSION['login']=true;
			$row = mysqli_fetch_row($res);
			$_SESSION['id']=$row[0];
			$_SESSION['Ime']=$row[4];
			$_SESSION['email']=$row[3];
			$_SESSION['Prezime']=$row[5];
			$_SESSION['user']=$_POST['user'];
			$_SESSION['pass']=$_POST['pass'];
		}
	}

	// odjava korisnika
	if(isset($_POST['logout'])){
		$bLoggedIn=false;
		session_unset();
		session_destroy();
		header("location:index.php");
		exit();
	}

	// registracija novog korisnika
	$_SESSION['KorisnikRegistriran']=false;
	$_SESSION['KorisnikNijeRegistriran']=false;
	if(isset($_POST['register'])){
		$bLoggedIn=true;
		$q="INSERT INTO korisnici VALUES (NULL,'" . md5($_POST['userReg']) . "','" . md5($_POST['passReg']) . "','{$_POST['email']}','{$_POST['Ime']}','{$_POST['Prezime']}');";
		$res = mysqli_query($con, $q);
		if(mysqli_errno($con)==1062){ // denied duplicate entry
			$_SESSION['KorisnikNijeRegistriran']=true;
			$bLoggedIn=false;
			session_unset();
			session_destroy();
			header("location:index.php");
			exit();
		}
		else{
			$_SESSION['KorisnikRegistriran']=true;
			// logiranje nakon registracije
			$q="SELECT * FROM korisnici WHERE(user='".md5($_POST['userReg'])."' and pass='".md5($_POST['passReg'])."')";
			$res = mysqli_query($con, $q);
			if(mysqli_num_rows($res)==1){
				$bLoggedIn = true;
				$_SESSION['login']=true;
				$row = mysqli_fetch_row($res);
				$_SESSION['id']=$row[0];
				$_SESSION['Ime']=$row[4];
				$_SESSION['Prezime']=$row[5];
				$_SESSION['email']=$row[3];
				$_SESSION['user']=$_POST['userReg'];
				$_SESSION['pass']=$_POST['passReg'];
			}
		}
	}

	// uređivanje podataka korisnika i ulogiravanje korisnika s novim podacima
	$_SESSION['KorisnikIzmijenjen']=false;
	$_SESSION['KorisnikNijeIzmijenjen']=false;
	if(isset($_POST['edit'])){
		//izmjena korisničkih podataka
		$v1 = fix_input($_POST["user"]);
		$v2 = fix_input($_POST["pass"]);
		$v3 = fix_input($_POST["email"]);
		$v4 = fix_input($_POST["Ime"]);
		$v5 = fix_input($_POST["Prezime"]);

		$q="UPDATE korisnici SET user='".md5($v1)."', pass='".md5($v2)."', email='{$v3}', Ime='{$v4}', Prezime='{$v5}' WHERE(id='{$_SESSION['id']}')";
		$res = mysqli_query($con, $q);

		if(mysqli_errno($con)==1062){ // denied duplicate entry
			$_SESSION['KorisnikNijeIzmijenjen']=true;
			header("location:index.php");
			exit();
		}
		else{
			$_SESSION['KorisnikIzmijenjen']=true;

			// logiranje nakon izmjena
			$q="SELECT * FROM korisnici WHERE(user='".md5($v1)."' and pass='".md5($v2)."')";
			$res = mysqli_query($con, $q);
			if(mysqli_num_rows($res)){
				$bLoggedIn = true;
				$_SESSION['login']=true;
				$row = mysqli_fetch_row($res);
				$_SESSION['id']=$row[0];
				$_SESSION['user']=$v1;
				$_SESSION['pass']=$v2;
				$_SESSION['email']=$row[3];
				$_SESSION['Ime']=$row[4];
				$_SESSION['Prezime']=$row[5];
			}
		}
	}

	// brisanje korisnika
	if(isset($_POST['delete'])){
		$q="CALL BrisanjeKorisnika({$_SESSION['id']});";
		mysqli_query($con, $q);

		$bLoggedIn=false;
		session_unset();
		session_destroy();
		header("location: index.php");
		exit();
	}

	// unos cvijeta
	$_SESSION['CvijetDodan']=false;
	$_SESSION['PovezanCvijetKorisnik']=false;
	if(isset($_POST["novi_cvijet"])){
		$ime   = fix_input($_POST["ime"]);
		$slika = fix_input($_POST["slika"]);
		$p1 	 = fix_input($_POST["period_ljeto"]);
		$p2 	 = fix_input($_POST["period_zima"]);
		$d1 	 = fix_input($_POST["datum_zaliven"]);

		$q="CALL UnosCvijeta('$ime','$slika',$p1,$p2,'$d1',".$_SESSION['id'].");";

		mysqli_query($con, $q);

		$_SESSION['CvijetDodan']=true;
		$_SESSION['PovezanCvijetKorisnik']=true;
	}

	// brisanje cvijeta
	$_SESSION['CvijetObrisan']=false;
	if(isset($_POST["delete_submit"])){
		$id = $_POST["delete_id"];

		$q="CALL BrisanjeCvijeta($id);";

		mysqli_query($con, $q);

		$_SESSION['CvijetObrisan']=true;
	}

	// zalijevanje cvijeta klikom na sliku
	$_SESSION['CvijetZaliven']=false;
	if(isset($_POST["plant_submit"])){
		$plant_id = $_POST["plant_id"];
		$plant_date = $_POST["plant_date"];

		$q="UPDATE cvijece SET datum_zaliven='" . $plant_date . "' WHERE id=" . $plant_id . ";";

		mysqli_query($con, $q);

		$_SESSION['CvijetZaliven']=true;

		// redirect to: bookmark (anchor)
		// redirect refreshes page. Only $_SESSION variable will survive.
		// $locationString = "Location: zalijevanje.php#A".$plant_id;
		// header($locationString);
	}

	// zalijevanje cvijeta promjenom datuma
	$_SESSION['IzmjenaDatumZaliven']=false;
	if(isset($_POST["plant_edit_submit"])){
		$plant_edit_id = $_POST["plant_edit_id"];
		$plant_edit_date = $_POST["plant_edit_date"];

		$q="UPDATE cvijece SET datum_zaliven='" . $plant_edit_date . "' WHERE id=" . $plant_edit_id . ";";

		mysqli_query($con, $q);

		$_SESSION['IzmjenaDatumZaliven']=true;

		// redirect to: bookmark (anchor)
		// redirect refreshes page. Only $_SESSION variable will survive.
		//$locationString = "Location: zalijevanje.php#A".$plant_edit_id;
		//header($locationString);
	}

	// zalijevanje cvijeta barkodom
	$_SESSION['CvijetZalivenBarkodom']=false;
	if(isset($_POST["barcode_submit"])){
		$barcode_id = $_POST["barcode_id"];
		$barcode_date = $_POST["barcode_date"];

		$q="UPDATE cvijece SET datum_zaliven='" . $barcode_date . "' WHERE id=" . $barcode_id . ";";

		mysqli_query($con, $q);

		$_SESSION['CvijetZalivenBarkodom']=true;

		// redirect to: bookmark (anchor)
		// redirect refreshes page. Only $_SESSION variable will survive.
		//$locationString = "Location: zalijevanje.php#A".$barcode_id;
		//header($locationString);
	}

	// povezivanje korisnika s postojećim cvijetom
	//$_SESSION['PovezanCvijetKorisnik']=false; //pozvano kod unosa novog cvijeta
	$_SESSION['NijePovezanCvijetKorisnik']=false;
	$_SESSION['CvijetNePostoji']=false;
	if(isset($_POST["link_barcode_submit"])){
		$link_barcode_id = $_POST["link_barcode_id"];
		$q="SELECT * FROM `cvijece` WHERE id=$link_barcode_id;";
		$res = mysqli_query($con, $q);
		if(mysqli_num_rows($res)==0){
			$_SESSION['CvijetNePostoji']=true;
		}
		else{
			$q="INSERT INTO zalijevanje VALUES(NULL,{$_SESSION['id']},{$link_barcode_id});";

			$res = mysqli_query($con, $q);

			if(mysqli_errno($con)==1062){ // denied duplicate entry
				$_SESSION['NijePovezanCvijetKorisnik']=true;
			}
			else{
				$_SESSION['PovezanCvijetKorisnik']=true;
			}
		}
	}

	// sortiranje
	$_SESSION['Sortirano'] = false;
	if(isset($_POST['sortSelect1'])){
		$order1 = $_POST["sortSelect1"];
		$_SESSION['order1']=$order1;
		$_SESSION['order2'] = 0;
		$_SESSION['Sortirano'] = true;
	}
	if(isset($_POST['sortSelect2'])){
		$order2 = $_POST["sortSelect2"];
		$_SESSION['order2']=$order2;
		$_SESSION['Sortirano'] = true;
	}
	if(!isset($_SESSION['order1'])) {
		$_SESSION['order1'] = 0;
	}
	if(!isset($_SESSION['order2'])) {
		$_SESSION['order2'] = 0;
	}
	$order1 = $_SESSION['order1'];
	$order2 = $_SESSION['order2'];
	$orderString = "";
	$orderString = $order1 == 0 ? "cvijece.ime" : $order1 == 1 ? "cvijece.ime ASC" : "cvijece.id ASC";
	$orderString2 = $order2 == 0 ? "" : $order2 == 1 ? ", cvijece.ime ASC" : ", cvijece.id ASC";
	$orderString = $orderString . $orderString2;

	function fix_input($data){
		$data = trim($data); // briše razmake na rubovima stringa. Primjer: " x " => "x"
		$data = stripslashes($data); // uklanja slash znak "O\'reilly" => "O'reilly"
		$data = htmlspecialchars($data); // popravlja zapis posebnih znakova: <, >, ', ", ...
		return $data;
	}
?>
