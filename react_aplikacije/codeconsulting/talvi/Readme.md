# TALVi

To run this application on Mac please follow these instructions.

* * *

### Database

To import mongo database, open a new tab in the terminal, navigate to the *database* folder and run run

    mongorestore -d glooko_logs -c log_entries /log_entries.bson

To activate database server run

    mongod

### Backend

Open a new tab in the terminal, navigate to the *backend* folder and run

    rails s

Rails is now at [http://localhost:3000](http://localhost:3000).

### Frontend

Open a new tab in the terminal, navigate to the *frontend* folder and run

    npm start

Answer the question with Y. React is now at [http://localhost:3001](http://localhost:3001).

### Chrome

Open a new tab in the terminal and run this command to disable CORS in Chrome

    killall -9 Google\ Chrome; open -a Google\ Chrome --args --disable-web-security --user-data-dir 'http://localhost:3001'

The following flag for increasing the maximum cache memory may be used when opening the Chrome from terminal.

    --disk-cache-size=1100000000

It will increase the maximum cache memory to 1GB. This value is displayed in the Chrome browser, in the field *Max size* at the URL: [chrome://net-internals/#httpCache](chrome://net-internals/#httpCache).

* * *

## Screenshots

* * *
![Alt text](/Screenshots/Screenshot001.png?raw=true "TALVi application in progress")
* * *
![Alt text](/Screenshots/Screenshot002.png?raw=true "TALVi application in progress")
* * *
![Alt text](/Screenshots/Screenshot003.png?raw=true "TALVi application in progress")
* * *
![Alt text](/Screenshots/Screenshot004.png?raw=true "TALVi application in progress")
* * *
