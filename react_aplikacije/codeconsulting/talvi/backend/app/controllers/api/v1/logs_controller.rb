module Api
  module V1
    class LogsController < ApplicationController

      # ISSUE: current api/v1/ ignores null (nil) field values
      # or({http_code: {'$ne': 0}, http_code: nil},
      # {http_code: {'$gte': @h, '$lte': @H}}).
        # {views_duration: {'$gte': @v}},
        # {total_duration: {'$gte': @t}},
        # {views_duration: {'$ne': 0}, views_duration: nil},
        # {total_duration: {'$ne': 0}, total_duration: nil}).

      def index # Initial fetch
        # http://localhost:3000/api/v1/logs?s="2017-05-24T23:17:00.000Z"&S="2017-05-24T23:55:00.000Z"&h=200&H=430&t=10&v=0&o=200&l=1000&b="timestamp ASC"&r=()
        @s = Time.parse(params[:s]); # s = start timestamp
        @S = Time.parse(params[:S]); # S = stop timestamp
        @h = params[:h].to_i;        # h = http_code minimum
        @H = params[:H].to_i;        # H = http_code maximum
        @l = params[:l].to_i;        # l = limit the number of documents
        @o = params[:o].to_i;        # o = offset (skip) the number of documents
        @t = params[:t].to_f;        # t = total_duration minimum
        @v = params[:v].to_f;        # v = views_duration minimum
        @b = params[:b].to_s;        # b = order by with the direction asc/desc
        @r = params[:r];             # r = regex for log message lines

        render json: LogEntry.
          between(timestamp: @s..@S).
          any_of(lines: Regexp.new(".*"+@r+".*")).
          between(http_code: @h..@H). # ignores http_code == nil
          gte(total_duration: @t).    # ignores total_duration == nil
          gte(views_duration: @v).    # ignores views_duration == nil
          order_by(@b).
          limit(@l), status: :ok
      end

      def ltmin # fetch less than minimum
        @f = Time.parse(params[:f]); # f = fetch timestamp
        @l = params[:l].to_i;        # l = limit the number of documents
        @b = params[:b].to_s;        # b = order by with the direction asc/desc
        render json: LogEntry.
          lt(timestamp: @f).
          order_by(@b).
          limit(@l), status: :ok
      end

      def gtmax # fetch gtreater than maximum
        @f = Time.parse(params[:f]); # f = fetch timestamp
        @l = params[:l].to_i;        # l = limit the number of documents
        @b = params[:b].to_s;        # b = order by with the direction asc/desc
        render json: LogEntry.
          gt(timestamp: @f).
          order_by(@b).
          limit(@l), status: :ok
      end
    end
  end
end
