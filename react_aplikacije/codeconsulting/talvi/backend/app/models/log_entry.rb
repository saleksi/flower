class LogEntry
  include Mongoid::Document
  field :http_code, type: Integer
  field :lines, type: String
  field :pid, type: Integer
  field :tid, type: String
  field :timestamp, type: Time
  field :total_duration, type: Float
  field :views_duration, type: Float
end
