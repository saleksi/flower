import React, { Component } from 'react';
import { ContentBox } from "./ContentBox";
import { LabeledInput, InputRow } from "./LabeledInput";
import { List, AutoSizer, InfiniteLoader } from 'react-virtualized';
import DatetimeRangePicker from 'react-bootstrap-datetimerangepicker';
import { Creatable } from 'react-select';
import VirtualizedSelect from 'react-virtualized-select';
import _ from 'lodash';

import { _getUrl, _getRanges, _rowClassName,_getDurationColor, _getTypeColor,
         Highlight, Skip} from './functions';
import { timestampRegex, dateTimeFormat, locale, filterTextOptions,
         sortNameOptions, sortDirectionOptions,
         STATUS_LOADING, STATUS_LOADED} from './constants';

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/css/bootstrap-theme.min.css';
import '../node_modules/bootstrap-daterangepicker/daterangepicker.css';
import './daterangepicker.css';
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import './index.css';

class App extends Component {
  constructor(props) {
    super(props);

    const ranges = _getRanges();
    const limitFetchItems = 1000;
    const loadedRows = new Array(limitFetchItems).fill(false);
    this.state = {
      items: [],
      loadingItems: true,
      overscanRowCount: 10,
      listWidth: 4000,
      loadedRows: loadedRows,
      rowCount: 0,
      rowCountPlus: 0,
      fetchLimit: limitFetchItems,
      fetchLimitPlus: 200,
      lineHeight: 20,
      messageLineLengthLimit: 3500,
      sortName: sortNameOptions[0].value,
      sortDirection: sortDirectionOptions[0].value,
      sortNameOptions: sortNameOptions,
      sortDirectionOptions: sortDirectionOptions,
      isSorted: false,
      startTimeStamp: ranges.Default[0],
      stopTimeStamp: ranges.Default[1],
      minHttpCode: 0,
      maxHttpCode: 999,
      minTotalDuration: 1000,
      minViewsDuration: 0,
      filterTextOptions: filterTextOptions,
      filterTextArray: [],
      shouldListRecomputeRowHeights: false,
    };

    this._loadMoreRows = this._loadMoreRows.bind(this);
    this._isRowLoaded = this._isRowLoaded.bind(this);
    this._handleApply = this._handleApply.bind(this);
    this._fetchData = this._fetchData.bind(this);
    this._fetchFilteredData = this._fetchFilteredData.bind(this);
    this._getRowHeight = this._getRowHeight.bind(this);
    this._getItems = this._getItems.bind(this);
    this._rowRenderer = this._rowRenderer.bind(this);
  }

  componentDidMount() {
    this._fetchData(true);
  }

  _fetchData(initialFetch) {
    const url = _getUrl(this.state, initialFetch);
    console.log(url);
    fetch(url).then(response => response.json()).then(data => {
      if(_.isEmpty(data)) {
        this.setState({
          items: [],
          loadingItems: false,
          rowCount: 0,
        });
        return;
      }
      const items = this._getItems(data);
      const rowCount = items.length;
      const loadedRows = [];
      _.map(this.state.loadedRows,(row,index)=>{
        const status = (index<rowCount) ? STATUS_LOADED : STATUS_LOADING;
        loadedRows.push(status)});

      this.setState({
        items: items,
        loadedRows: loadedRows,
        rowCount: rowCount,
        isSorted: true,
        loadingItems: false,
      });
    });
  }
  _getItems(data) {
    const {lineHeight, messageLineLengthLimit} = this.state;
    const items = [];
    for(let i=0; i<data.length; ++i) {
      const dataItem = data[i];
      const id = dataItem._id.$oid;
      const pid = dataItem.pid;
      const tid = dataItem.tid;
      const firstTimestamp = dataItem.timestamp;
      const totalDuration = parseInt(dataItem.total_duration,10);
      const viewsDuration = parseInt(dataItem.views_duration,10);
      const httpCode = parseInt(dataItem.http_code,10);
      // fix invisible characters \s=[\r\n\t\f\v ] with the space character
      const lines = _.replace(dataItem.lines, /\s+/g, (match)=>(" "));
      const lineRegex = new RegExp("[IDWEFA],\\s{1}\\[\\d{4}-\\d{2}-\\d{2}T"+
        "\\d{2}:\\d{2}:\\d{2}\\.\\d{6}\\s{1}#"+dataItem.pid+"\\s{1}\\$"+dataItem.tid+
        "\\]\\s{1}[A-Z]+\\s{1}--\\s{1}:\\s{1}","g");
      const message = lines.split(lineRegex).filter(value => value);
      const lineLengthSkip = _.map(message, (val, index)=>{
        const lineLength = message[index].length;
        if(lineLength < messageLineLengthLimit) {
          return 0;
        } else {
          return lineLength;
      }});
      const messageType = _.map(lines.match(lineRegex),(val)=>(val.charAt(0)));
      const timestamp = lines.match(timestampRegex);
      const messageCount = message.length;
      const rowHeight = _.max([messageCount, 7]) * lineHeight;
      const row = this._rowPreRender(id,pid,tid,httpCode,totalDuration,viewsDuration,
        timestamp,message,messageType,lineLengthSkip);
      items.push({id,pid,tid,firstTimestamp,lineLengthSkip,totalDuration,row,
        timestamp,messageType,httpCode,message,messageCount,rowHeight,viewsDuration});
    }
    return items;
  }
  _rowPreRender( id, pid, tid, httpCode, totalDuration, viewsDuration,
                 timestamp, message, messageType, lineLengthSkip ) {
    return (
      <div>
        <div className="listItem w1">
          <span className="objectID">{id}<br/></span>
          {pid}<br/>{tid}<br/>{httpCode}<br/>
          <span className={_getDurationColor(totalDuration)}>
          {totalDuration+" ms"}</span><br/>{viewsDuration+" ms"}
        </div>
        <div className="listItem w2">
          {_.map(timestamp, ((value,i) => (
            <span key={"A"+i}>{value}<br/></span>)))}
        </div>
        <div className="listItem w3">
          {_.map(messageType, ((value,i) => (
            <div  key={"B"+i} className={"letter "+
              _getTypeColor(value)}>{value}<br/></div>)))}
        </div>
        <div className="listItem w4" style={{width: "100%", overflow: "scroll"}}>
          {_.map(message,(value,index) => lineLengthSkip[index]
              ? <Skip  index={index} length={lineLengthSkip[index]} id={id} />
              : <Highlight key={"H"+index} singleLine={value} /> )}
        </div>
      </div>);
  }

  _rowRenderer({ index, key, style }) {
    const isRowLoaded = this.state.loadedRows[index];
    return (
      isRowLoaded ?
        <div className={_rowClassName(index)} key={key} style={style}>
            {this.state.items[index].row}</div>
        : <div className={_rowClassName(index)} key={key} style={style}>
            Loading row...</div>
    );
  }
  render() {
    const{items, loadingItems, rowCount, listWidth, overscanRowCount,
          startTimeStamp, stopTimeStamp, minTotalDuration, minViewsDuration,
          minHttpCode, maxHttpCode, filterTextOptions, filterTextArray,
          sortDirection, sortDirectionOptions, sortName, sortNameOptions } = this.state;
    const ranges = _getRanges();
    const itemsReady = !_.isEmpty(items);
    const numberOfLoadedRows = itemsReady ? rowCount : 0;
    const dateTimePickerValue = startTimeStamp.format(dateTimeFormat) + " - " +
                                stopTimeStamp.format(dateTimeFormat);
    return (
      <ContentBox>
        <InputRow>
          <div className="LabeledInput w5">
            <label className="Label">{"Date-Time range"}</label>
            <DatetimeRangePicker timePicker timePicker24Hour timePickerSeconds
               locale={locale} ranges={ranges} onApply={this._handleApply}
               startDate={startTimeStamp} endDate={stopTimeStamp}
               autoUpdateInput={true} autoApply={true}>
                <input name="dtrPicker" className="Input w5" type="text"
                  readOnly value={dateTimePickerValue} />
            </DatetimeRangePicker>
          </div>
          {this._renderLabeledInput(minTotalDuration,"Total [ms]","minTotalDuration")}
          {this._renderLabeledInput(minViewsDuration,"Views [ms]","minViewsDuration")}
          {this._renderLabeledInput(minHttpCode,"min. HTTP","minHttpCode")}
          {this._renderLabeledInput(maxHttpCode,"max. HTTP","maxHttpCode")}
          {this._renderLabeledInput(listWidth,"List width","listWidth")}
          {this._renderLabeledInput(overscanRowCount,"Overscan","overscanRowCount")}
        </InputRow>
        <InputRow>
          <button className="buttonStyle" type="submit" name="fetchFilteredData"
            onClick={this._fetchFilteredData}>Fetch filtered data</button>
          <VirtualizedSelect labelKey='label' optionHeight={40} clearable={false}
            onChange={(sortName) => this.setState({ sortName: sortName })}
            options={sortNameOptions} selectComponent={Creatable} simpleValue
            value={sortName} valueKey='value' style={{width: '150px', height: '30px', marginRight: "10px"}}/>
          <VirtualizedSelect labelKey='label' optionHeight={40} clearable={false}
            onChange={(sortDirection) => this.setState({ sortDirection: sortDirection })}
            options={sortDirectionOptions} selectComponent={Creatable} simpleValue
            value={sortDirection} valueKey='value' style={{width: '120px', height: '30px', marginRight: "10px"}}/>
          <VirtualizedSelect labelKey='label' multi clearable optionHeight={40}
            onChange={(valueArray) => this.setState({filterTextArray: valueArray})}
            options={filterTextOptions} selectComponent={Creatable}
            value={filterTextArray} valueKey='value'
            style={{width: '500px', height: '30px', owerflowY: 'hidden', marginRight: "10px"}}/>
          <div className="numberOfRows">Loaded {numberOfLoadedRows} rows</div>
        </InputRow>
        <div className="AutoSizerWrapper">
          {(itemsReady)
            ? <InfiniteLoader isRowLoaded={this._isRowLoaded}
                loadMoreRows={this._loadMoreRows} rowCount={rowCount}>
                  {({ onRowsRendered, registerChild }) =>
                    <AutoSizer disabledWidth>{({height}) =>
                      <List ref={registerChild}
                        className = "List"
                        width={listWidth}
                        height={height}
                        overscanRowCount={overscanRowCount}
                        rowCount={rowCount}
                        rowHeight={this._getRowHeight}
                        rowRenderer={this._rowRenderer}
                        onRowsRendered={onRowsRendered}
                      />}</AutoSizer>}</InfiniteLoader>
            : <InputRow><div className="noRows">
                {(loadingItems) ? "Loading..." : "There are no logs to show."}
              </div></InputRow>}
        </div>
      </ContentBox>
    );
  }
  _renderLabeledInput(value, label, name) {
    return (
      <LabeledInput value={value} label={label} name={name}
      onChange={event =>this.setState({ [name]: Number(event.target.value)})}/>
    );
  }
  _getRowHeight({index}) {
    const item = this.state.items[index];
    return item.rowHeight;
  }
  _isRowLoaded({index}) {
    const { loadedRows } = this.state;
    return !!loadedRows[index];
  }
  _loadMoreRows({ startIndex, stopIndex }) {
    //const {sortDirection} = this.state;

    // M = rowCount
    // N = rowCountPlus
    // limitM = fetchLimit
    // limitN = fetchLimitPlus

    //const scrollDirectionUp = true; // how to detect scroll direction?
    console.log(startIndex);
    //this._fetchData(false);
  }
  _handleApply(event, picker) {
    const startTimeStamp = picker.startDate;
    const stopTimeStamp = picker.endDate;
    this.setState({
      startTimeStamp: startTimeStamp,
      stopTimeStamp: stopTimeStamp,
    });
  }
  _fetchFilteredData() {
    this.setState({
      items: [],
      loadingItems: true,
    });
    this._fetchData();
  }
}

export default App;
