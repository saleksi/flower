import PropTypes from "prop-types";
import React from "react";
import "./index.css";

export function LabeledInput({
  disabled,
  label,
  name,
  onChange,
  placeholder,
  value,
  className
}){
  const labelClassName = disabled ? "Label LabelDisabled" : "Label";
  const labeledInputClassName = "LabeledInput " + className;
  return (
    <div className={labeledInputClassName}>
      <label className={labelClassName} title={label}>
        {label}
      </label>
      <input
        aria-label={label}
        className="Input"
        name={name}
        placeholder={placeholder}
        onChange={onChange}
        value={value}
        disabled={disabled}
      />
    </div>
  );
}
LabeledInput.propTypes = {
  disabled: PropTypes.bool,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.any,
  className: PropTypes.any
};

export function InputRow({ children }) {
  return (
    <div className="InputRow">
      {children}
    </div>
  );
}
