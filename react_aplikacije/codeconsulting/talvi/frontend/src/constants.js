import moment from 'moment';

export const highlightRegex = /(GET|POST|PUT|DELETE)|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(Processing by [^#]+#\w+ as \w+\s)/g; // highlightRegex = method | ipAddress | controller , action
export const timestampRegex = /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{6}/g;
export const dateTimeFormat = "YYYY-MM-DD HH:mm:ss";

export const utcOffset = moment().utcOffset();

export const locale = {
    format: dateTimeFormat,
    applyLabel: 'Apply',
    cancelLabel: 'Clear',
    weekLabel: 'W',
    customRangeLabel: 'Custom Range',
  };

export const filterTextOptions = [
  { label: 'GET'    , value: 'GET'     },
  { label: 'POST'   , value: 'POST'    },
  { label: 'PUT'    , value: 'PUT'     },
  { label: 'DELETE' , value: 'DELETE'  },
  { label: 'INFO'   , value: 'INFO'    },
  { label: 'DEBUG'  , value: 'DEBUG'   },
  { label: 'WARNING', value: 'WARNING' },
  { label: 'ERROR'  , value: 'ERROR'   },
  { label: 'FATAL'  , value: 'FATAL'   },
  { label: 'UNKNOWN', value: 'UNKNOWN' },
  { label: 'pdf'    , value: 'pdf'     },
];

export const sortNameOptions = [
  { label: 'Timestamp'      , value: 'timestamp'      },
  { label: 'Total duration' , value: 'total_duration' },
  { label: 'Views duration' , value: 'views_duration' },
  { label: 'HTTP code'      , value: 'http_code'      },
];

export const sortDirectionOptions = [
  { label: 'ascending'  , value: true  },
  { label: 'descending' , value: false },
];

export const skippedTextPart = [
  "<span style=\"font-size: bold; color: blue;\">Line is too long (",
  " characters). Please view this document with the following query {\"_id\":{\"$oid\":\"",
  "\"}}<br/></span>"];

export const highlightedTextPart = [
  "<span key={\"C\"+i}>",
  "<span style=\"font-size: bold; color: white; background-color: black;\">",
  "</span>",
  "<br/></span>"];

export const STATUS_LOADING = 1;
export const STATUS_LOADED = 2;
