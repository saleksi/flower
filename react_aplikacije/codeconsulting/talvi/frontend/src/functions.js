import React from 'react';
import moment from 'moment';
import _ from 'lodash';

import {highlightRegex, utcOffset} from './constants';

export function _getTypeColor(value) {
  switch(value) {
    case 'I': return "b1";
    case 'D': return "b2";
    case 'W': return "b3";
    case 'E': return "b4";
    case 'F': return "b5";
    case 'A': return "b6";
    default:  return "";
}}

export function _getDurationColor(value) {
  if(value<=50)        return "duration c1";
  else if(value<=500)  return "duration c2";
  else if(value<=1000) return "duration c3";
  else if(value<=2000) return "duration c4";
  else /* 2000<value */return "duration c5";
}

export function _rowClassName(i) {
  return i % 2 === 0 ? "listContainer evenRow" : "listContainer oddRow";
}

export function _getRanges() {
  const ranges = {
    'Default': [moment("2017-05-24 23:17:00.0000Z").subtract(utcOffset,'minutes'),
                moment("2017-05-24 23:50:00.0000Z").subtract(utcOffset,'minutes')],
    'Last hour': [moment().subtract(1,'hours'), moment()],
    'Two hours': [moment().subtract(2,'hours'), moment()],
    'Today': [moment().startOf('day'), moment().endOf('day')],
  };
  return ranges;
}

export function _getUrl(state, initialFetch, scrollDirection) {
  const start = moment(state.startTimeStamp).add(utcOffset,'minutes').toISOString();
  const stop = moment(state.stopTimeStamp).add(utcOffset,'minutes').toISOString();
  const filterArray = _.map(state.filterTextArray, x => x.value);
  const filterText = "("+_.join(filterArray,")|(")+")";
  const limit = initialFetch ? state.fetchLimit : state.fetchLimitPlus;
  const currentSortDirection = initialFetch ? state.sortDirection : !state.sortDirection;
  const sortDirectionLabel = currentSortDirection ? "asc" : "desc";
  const url = "http://localhost:3000/api/v1/logs?" +
              "s=\"" + start + "\"" +
              "&S=\"" + stop + "\"" +
              "&h=" + state.minHttpCode +
              "&H=" + state.maxHttpCode +
              "&t=" + state.minTotalDuration +
              "&v=" + state.minViewsDuration +
              "&l=" + limit +
              "&b=" + state.sortName + " " + sortDirectionLabel  +
              "&r=" + filterText;
  return url;
}

export function Skip(props){
  const {index, length, id} = props;
  return (
    <span key={"s"+index} className="skippedText">
    {"Line with "+length+" characters is too long to show. "+
    "Please view this document with the following query: "+
    "{\"_id\":{\"$oid\":\""+id+"\"}}"}<br/></span>
  );
}

export function Highlight(props) {
  const {singleLine} = props;
  const XX = singleLine.match(highlightRegex);
  const YY = singleLine.replace(highlightRegex,match=>"\n").split("\n").filter(value => value);
  const X = _.isEmpty(XX) ? [] : XX.filter(value => value);
  const Y = _.isEmpty(YY) ? [] : YY.filter(value => value);
  const lengthX = _.isEmpty(X) ? 0 : X.length;
  const lengthY = _.isEmpty(Y) ? 0 : Y.length;
  const diff = lengthY - lengthX;
  const startIndex = lengthX ? singleLine.indexOf(X[0],0) : 1;
  const firstX = diff<0 ? true : startIndex===0 ? true : false;
  const lineOutput = [];
  const styleOutput = [];
  const length = lengthX + lengthY;
  for(let i=0; i<length; i++) {
    let j = parseInt(i/2,10);
    lineOutput.push((i%2===0) ? firstX ? X[j] : Y[j] : firstX ? Y[j] : X[j]);
    styleOutput.push((i%2===0) ? firstX ? true : false : firstX ? false : true);
  }
  //console.log({startIndex,X,Y,lengthY,lengthX,diff,length,firstX,lineOutput,styleOutput});
  return (
    <span key={"c"}>
      {_.map(lineOutput,(value,i)=><span key={"s"+i} className={styleOutput[i] ?
        "highlightedText" : "regularText"}>{value}</span>)}
    <br/></span>
  );
}
