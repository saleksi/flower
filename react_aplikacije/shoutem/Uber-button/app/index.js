import UberEventDetails from './screens/UberEventDetails';
import UberPlaceDetails from './screens/UberPlaceDetails';

export const screens = {
  UberEventDetails,
  UberPlaceDetails,
};
