import React from 'react';
import {
  Linking,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';

import { connectStyle } from '@shoutem/theme';
import {
  ScrollView,
  View,
  Button,
  Text,
  Row,
  Image,
} from '@shoutem/ui';
import { navigateTo as navigateToAction } from '@shoutem/core/navigation';

import { openURL as openUrlAction } from 'shoutem.web-view';
import { DetailsScreenWithLargePhoto } from 'shoutem.events/screens/DetailsScreenWithLargePhoto';

import { ext } from '../const';


export class UberEventDetails extends DetailsScreenWithLargePhoto {

  constructor(props, context) {
    super(props, context);
    this.openUberLink = this.openUberLink.bind(this);
  }

  openUberLink() {
    const { location = {} } = this.props.event;
    const { latitude, longitude, formattedAddress } = location;

    const resolvedScheme = `uber://?client_id=lfOAomO50Sjm3yemwlN1wLF2_ptlhlpA&action=setPickup&pickup=my_location&dropoff[latitude]=${latitude}&dropoff[longitude]=${longitude}&dropoff[formatted_address]=${formattedAddress}`;
    const store = (Platform.OS === 'ios') ? `itms-apps://itunes.apple.com/us/app/uber/id368677368?mt=8` : `market://details?id=com.ubercab`;

    if (latitude && longitude) {
      Linking.canOpenURL(resolvedScheme).then((supported) => {
        if (supported) {
          Linking.openURL(resolvedScheme);
        } else {
          Linking.openURL(store);
        }
      });
    }
  }

  renderUberButton() {
    return (
      <Row>
        <View styleName="horizontal h-center">
          <Button styleName="secondary" onPress={this.openUberLink}>
            <Image style={{ width: 25, height: 25 }} source={require('../assets/images/uber-rubicon.png')} />
            <Text styleName="md-gutter-left">CATCH A RIDE</Text>
          </Button>
        </View>
      </Row>
    );
  }

  renderData(event) {
    return (
      <ScrollView>
        {this.renderHeader(event)}
        {this.renderRsvpButton(event)}
        {this.renderInformation(event)}
        {this.renderUberButton()}
        {this.renderMap(event)}
      </ScrollView>
    );
  }
}

export const mapDispatchToProps = {
  openURL: openUrlAction,
  navigateTo: navigateToAction,
};

export default connect(undefined, mapDispatchToProps)(
  connectStyle(ext('UberEventDetails'))(UberEventDetails),
);
