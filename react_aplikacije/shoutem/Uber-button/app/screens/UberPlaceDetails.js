import React from 'react';
import { connect } from 'react-redux';
import {
  Linking,
  Platform,
} from 'react-native';

import {
  Row,
  Text,
  View,
  Button,
  Image,
} from '@shoutem/ui';
import { navigateTo } from '@shoutem/core/navigation';
import { connectStyle } from '@shoutem/theme';

import { openURL } from 'shoutem.web-view';
import { PlaceDetails } from 'shoutem.places/screens/PlaceDetails';

import { ext } from '../const';

export class UberPlaceDetails extends PlaceDetails {

  constructor(props) {
    super(props);
    this.openUberLink = this.openUberLink.bind(this);
  }

  openUberLink() {
    const { location = {} } = this.props.place;
    const { latitude, longitude, formattedAddress } = location;

    const resolvedScheme = `uber://?client_id=lfOAomO50Sjm3yemwlN1wLF2_ptlhlpA&action=setPickup&pickup=my_location&dropoff[latitude]=${latitude}&dropoff[longitude]=${longitude}&dropoff[formatted_address]=${formattedAddress}`;
    const store = (Platform.OS === 'ios') ? `itms-apps://itunes.apple.com/us/app/uber/id368677368?mt=8` : `market://details?id=com.ubercab`;

    if (latitude && longitude) {
      Linking.canOpenURL(resolvedScheme).then((supported) => {
        if (supported) {
          Linking.openURL(resolvedScheme);
        } else {
          Linking.openURL(store);
        }
      });
    }
  }

  renderUberButton() {
    const { place } = this.props;
    const buttonStyle = place.rsvpLink ? 'md-gutter-left secondary' : 'secondary';
    return place.location ? (
      <Button styleName={buttonStyle} onPress={this.openUberLink}>
        <Image style={{ width: 25, height: 25 }} source={require('../assets/images/uber-rubicon.png')} />
        <Text styleName="md-gutter-left">CATCH A RIDE</Text>
      </Button>
    ) : null;
  }

  renderButtons() {
    const { place } = this.props;
    return (
      <Row>
        <View styleName="horizontal h-center">
          {this.renderRsvpButton(place)}
          {this.renderUberButton()}
        </View>
      </Row>
    );
  }
}

export default connect(undefined, { navigateTo, openURL })(
    connectStyle(ext('UberPlaceDetails'))(UberPlaceDetails),
  );
