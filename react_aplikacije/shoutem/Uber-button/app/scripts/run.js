const fs = require('fs-extra');
const plist = require('plist');

const infoPlistPath = 'ios/ShoutemApp/Info.plist';
const infoPlistFile = fs.readFileSync(infoPlistPath, 'utf8');
const infoPlist = plist.parse(infoPlistFile);

infoPlist.LSApplicationQueriesSchemes.push('itms-apps', 'uber');
fs.writeFileSync(infoPlistPath, plist.build(infoPlist));
