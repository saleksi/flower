import rio from '@shoutem/redux-io';

import { WORDPRESS_NEWS_SCHEMA } from './redux';

export function appDidMount() {
  rio.registerSchema({
    schema: WORDPRESS_NEWS_SCHEMA,
    request: {
      endpoint: '{feedUrl}/wp-json/wp/v2/posts',
      headers: {
        'Access-Control-Request-Method': 'application/json',
      },
    },
  });
}
