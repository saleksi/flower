import { combineReducers } from 'redux';
import _ from 'lodash';

import { resource, find } from '@shoutem/redux-io';

import { ext } from './const';

export const WORDPRESS_NEWS_SCHEMA = 'shoutem.wordpress.news';

export default combineReducers({
  news: resource(WORDPRESS_NEWS_SCHEMA),
});

export function getNewsFeed(state) {
  return _.get(state[ext()], 'news');
}

export function loadFeed(feedUrl) {
  const config = {
    schema: WORDPRESS_NEWS_SCHEMA,
    request: {
      endpoint: `${feedUrl}/wp-json/wp/v2/posts`,
      resourceType: 'json',
      headers: {
        'Access-Control-Request-Method': 'application/json',
      },
    },
  };
  return find(config, 'feedItems');
}
