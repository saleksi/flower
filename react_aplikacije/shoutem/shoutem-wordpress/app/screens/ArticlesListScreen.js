import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';

import { connectStyle } from '@shoutem/theme';
import { navigateTo as navigateToAction } from '@shoutem/core/navigation';
import { find, next } from '@shoutem/redux-io';

import { RssListScreen, getLeadImageUrl } from 'shoutem.rss';
import { ListArticleView } from 'shoutem.news';

import { ext } from '../const';
import { WORDPRESS_NEWS_SCHEMA, getNewsFeed, loadFeed, getUsers } from '../redux';

export class ArticlesListScreen extends RssListScreen {
  static propTypes = {
    ...RssListScreen.propTypes,
    loadFeed: React.PropTypes.func,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      ...this.state,
      schema: WORDPRESS_NEWS_SCHEMA,
    };

    this.openArticle = this.openArticle.bind(this);
    this.openArticleWithId = this.openArticleWithId.bind(this);
    this.renderRow = this.renderRow.bind(this);
  }

  openArticle(article) {
    const { navigateTo } = this.props;
    const nextArticle = this.getNextArticle(article);

    const route = {
      screen: ext('ArticleDetailsScreen'),
      title: article.title.rendered,
      props: {
        article,
        nextArticle,
        openArticle: this.openArticle,
      },
    };

    navigateTo(route);
  }

  fetchFeed() {
    const { feedUrl } = this.props;

    if (_.isEmpty(feedUrl)) {
      return;
    }

    this.props.loadFeed(feedUrl);
  }

  openArticleWithId() {
    const { feed } = this.props;
    const article = _.find(feed);
    this.openArticle(article);
  }

  getNextArticle(article) {
    const { feed } = this.props;
    const currentArticleIndex = _.findIndex(feed, { id: article.id });
    return feed[currentArticleIndex + 1];
  }

  renderRow(article) {
    return (
      <ListArticleView
        key={article.id}
        articleId={article.id.toString()}
        title={article.title.rendered}
        imageUrl={getLeadImageUrl(article)}
        date={article.date}
        onPress={this.openArticleWithId}
      />
    );
  }
}

export const mapStateToProps = (state, ownProps) => {
  const feedUrl = _.get(ownProps, 'shortcut.settings.feedUrl');

  return {
    feedUrl,
    feed: getNewsFeed(state),
  };
};

export const mapDispatchToProps = { navigateTo: navigateToAction, loadFeed, getUsers, find, next };

export default connect(mapStateToProps, mapDispatchToProps)(
  connectStyle(ext('ArticlesListScreen'))(ArticlesListScreen),
);
