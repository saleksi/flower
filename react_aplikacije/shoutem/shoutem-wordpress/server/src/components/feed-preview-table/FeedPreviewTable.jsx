import React, { PropTypes } from 'react';
import { isBusy } from '@shoutem/redux-io';
import './style.scss';

export default function FeedPreviewTable(props) {
  const { feedItems } = props;
  const loading = isBusy(feedItems);

  return (
    <table className="table feed-preview-table">
      <thead>
        <tr>
          <th className="feed-preview__table-date">Date</th>
          <th className="feed-preview__table-title">Title</th>
        </tr>
      </thead>
      <tbody>
        {loading && (
          <tr>
            <td colSpan="2">loading content...</td>
          </tr>
        )}
        {!loading && (feedItems.length === 0) && (
          <tr>
            <td colSpan="2">No content to show</td>
          </tr>
        )}
        {!loading && feedItems.map(item => (
          <tr key={item.id}>
            <td>
              <span title={item.dateTimeFormatted}>{item.dateTimeDisplay}</span>
            </td>
            <td>{item.title.rendered}</td>
          </tr>))}
      </tbody>
    </table>
  );
}

FeedPreviewTable.propTypes = {
  feedItems: PropTypes.array,
};
