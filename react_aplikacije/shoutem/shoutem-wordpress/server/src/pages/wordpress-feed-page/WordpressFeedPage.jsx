import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import { fetchShortcut, updateShortcutSettings, getShortcut } from '@shoutem/api';
import { shouldRefresh, clear, isBusy } from '@shoutem/redux-io';
import { connect } from 'react-redux';
import {
  FEED_ITEMS,
  loadFeed,
  getFeedItems,
} from '../../redux';
import FeedUrlInput from '../../components/feed-url-input';
import FeedPreview from '../../components/feed-preview';
import './style.scss';
import ext from '../../const';
import { validateWordpressUrl } from '../../services/wordpress';

const ACTIVE_SCREEN_INPUT = 0;
const ACTIVE_SCREEN_PREVIEW = 1;

class WordpressFeedPage extends Component {
  static propTypes = {
    shortcut: PropTypes.object,
    fetchShortcut: PropTypes.func,
    updateShortcutSettings: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.getActiveScreen = this.getActiveScreen.bind(this);
    this.saveFeedUrl = this.saveFeedUrl.bind(this);
    this.handleFeedUrlInputContinueClick = this.handleFeedUrlInputContinueClick.bind(this);
    this.handleFeedPreviewRemoveClick = this.handleFeedPreviewRemoveClick.bind(this);

    props.fetchShortcut();

    this.state = {
      error: null,
      hasChanges: false,
      feedUrl: _.get(props.shortcut, 'settings.feedUrl'),
    };
  }

  componentWillReceiveProps(nextProps) {
    const { feedUrl } = this.state;
    const { shortcut } = this.props;
    const { shortcut: nextShortcut } = nextProps;
    const { loadFeed } = nextProps;

    if (_.isEmpty(feedUrl)) {
      const feedUrl = _.get(nextShortcut, 'settings.feedUrl');
      this.setState({
        feedUrl,
      });
      if (validateWordpressUrl(feedUrl)) {
        loadFeed(feedUrl);
      }
    }

    if (
     shortcut !== nextShortcut &&
     shouldRefresh(nextShortcut)
   ) {
      this.props.fetchShortcut();
    }
  }

  getActiveScreen() {
    const { feedUrl } = this.state;
    if (!_.isEmpty(feedUrl)) {
      return ACTIVE_SCREEN_PREVIEW;
    }
    return ACTIVE_SCREEN_INPUT;
  }

  saveFeedUrl(feedUrl) {
    const { shortcut } = this.props;
    const settings = { feedUrl };

    this.setState({ error: '', inProgress: true });
    this.props.updateShortcutSettings(shortcut, settings).then(() => {
      this.setState({ hasChanges: false, inProgress: false });
    }).catch((err) => {
      this.setState({ error: err, inProgress: false });
    });
  }

  handleFeedUrlInputContinueClick(feedUrl) {
    this.setState({
      feedUrl,
    });
    this.saveFeedUrl(feedUrl);
    this.props.loadFeed(feedUrl);
  }

  handleFeedPreviewRemoveClick() {
    this.setState({
      feedUrl: null,
    });

    this.saveFeedUrl(null);
    this.props.clearFeedItems();
  }

  render() {
    const activeScreen = this.getActiveScreen();
    const { feedUrl } = this.state;
    const { feedItems } = this.props;

    return (
      <div className="wordpress-feed-page">
        {(activeScreen === ACTIVE_SCREEN_INPUT) && (
          <FeedUrlInput
            inProgress={isBusy(feedItems)}
            error={this.state.error}
            onContinueClick={this.handleFeedUrlInputContinueClick}
          />
        )}
        {(activeScreen === ACTIVE_SCREEN_PREVIEW) && (
          <div>
            <FeedPreview
              feedUrl={feedUrl}
              feedItems={feedItems}
              onRemoveClick={this.handleFeedPreviewRemoveClick}
            />
          </div>
        )}
      </div>
    );
  }
}

WordpressFeedPage.propTypes = {
  shortcut: PropTypes.object,
  feedItems: PropTypes.array,
  fetchShortcut: PropTypes.func,
  updateShortcutSettings: PropTypes.func,
  loadFeed: PropTypes.func,
  clearFeedItems: PropTypes.func,
};

function mapStateToProps(state, ownProps) {
  const { shortcutId } = ownProps;
  const extensionName = ext();

  return {
    shortcut: getShortcut(state, shortcutId),
    feedItems: getFeedItems(state, extensionName, shortcutId),
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  const { shortcutId } = ownProps;

  return {
    fetchShortcut: () => dispatch(fetchShortcut(shortcutId)),
    updateShortcutSettings: (shortcut, { feedUrl }) => (
      dispatch(updateShortcutSettings(shortcut, { feedUrl }))),
    clearFeedItems: () => dispatch(clear(FEED_ITEMS, 'feedItems')),
    loadFeed: url => dispatch(loadFeed(url, shortcutId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WordpressFeedPage);
