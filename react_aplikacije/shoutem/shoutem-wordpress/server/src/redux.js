import { createScopedReducer, getShortcutState } from '@shoutem/api';
import { resource, find, cloneStatus, RESOLVED_ENDPOINT } from '@shoutem/redux-io';
import _ from 'lodash';
import { toLocalDateTime } from './services/wordpress';

export const FEED_ITEMS = 'shoutem.wordpress.feedItems';

export default createScopedReducer({
  shortcut: {
    feedItems: resource(FEED_ITEMS),
  },
});

const resolvedEndpointOptions = {
  [RESOLVED_ENDPOINT]: true,
};

export function loadFeed(feedUrl, shortcutId) {
  const config = {
    schema: FEED_ITEMS,
    request: {
      endpoint: `${feedUrl}/wp-json/wp/v2/posts`,
      resourceType: 'json',
      headers: {
        'Access-Control-Request-Method': 'application/json',
      },
    },
  };
  return find(config, 'feedItems', { shortcutId }, resolvedEndpointOptions);
}

function createFeedItemInfo(feedItem) {
  const dateTime = toLocalDateTime(feedItem.date);
  const { dateTimeDisplay, dateTimeFormatted } = dateTime;
  const feedItemInfo = {
    ...feedItem,
    dateTimeFormatted,
    dateTimeDisplay,
  };
  cloneStatus(feedItem, feedItemInfo);
  return feedItemInfo;
}

export function getFeedItems(state, extensionName, shortcutId) {
  const shortcutState = getShortcutState(state, extensionName, shortcutId);
  const feedItems = _.get(shortcutState, 'feedItems');

  if (!feedItems) {
    return null;
  }

  const feedItemInfos = _.map(feedItems, item => createFeedItemInfo(item));
  cloneStatus(feedItems, feedItemInfos);
  return feedItemInfos;
}
