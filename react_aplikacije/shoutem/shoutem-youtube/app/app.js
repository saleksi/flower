import rio from '@shoutem/redux-io';
import { YOUTUBE_VIDEOS_SCHEMA } from './redux';

export function appDidMount() {
  rio.registerSchema({
    schema: YOUTUBE_VIDEOS_SCHEMA,
    request: {
      endpoint: 'https://www.googleapis.com/youtube/v3/{type}',
      resourceType: 'json',
      headers: {
        'Content-Type': 'application/json',
      },
    },
  });
}
