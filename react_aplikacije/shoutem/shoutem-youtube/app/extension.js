// This file is managed by Shoutem CLI
// You should not change it
import pack from './package.json';
import SmallYoutubeList from './screens/SmallYoutubeList';
import YoutubeDetails from './screens/YoutubeDetails';
import YoutubeList from './screens/YoutubeList';

export const screens = {
  SmallYoutubeList,
  YoutubeList,
  YoutubeDetails,
};

export function ext(resourceName) {
  return resourceName ? `${pack.name}.${resourceName}` : pack.name;
}
