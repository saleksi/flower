import { combineReducers } from 'redux';
import { resource, find } from '@shoutem/redux-io';
import _ from 'lodash';

import { resolveUserPlaylistUrl } from './services/user-uploads';
import { ext } from './const';

export const YOUTUBE_VIDEOS_SCHEMA = 'shoutem.youtube.videos';

export default combineReducers({
  videos: resource(YOUTUBE_VIDEOS_SCHEMA),
});

export function getVideosFeed(state) {
  return _.get(state[ext()], 'videos');
}

const sharedParams = {
  part: 'snippet',
  maxResults: 20,
};

function extractChannelId(feedUrl) {
  // eslint-disable-next-line max-len
  return feedUrl.match(/(?:http|https:\/\/|)www.youtube\.com\/channel\/([a-zA-Z0-9]{1,}_[a-zA-Z0-9]{1,})/i)[1];
}
// This regex will extract the parameter id from a YouTube channel link, for example,
// for the link https://www.youtube.com/channel/UCrJs_gMaJZDqN6Wz76FQ35A,
// regex will result with UCrJs_gMaJZDqN6Wz76FQ35A.

function extractPlaylistId(feedUrl) {
  return feedUrl.match(/^.*(?:youtu.be\/|list=)([^#]*).*/i)[1];
}

// This regex will extract the parameter id from a YouTube playlist link, for example,
// for the link https://www.youtube.com/watch?v=2jAoKF2heDE&list=PLjVnDc2oPyOGBOb75V8CpeSr9Gww8pZdL,
// regex will result with UCrJs_gMaJZDqN6Wz76FQ35A.

function fetchUserPlaylistId(feedUrl, apiKey) {
  const endpoint = resolveUserPlaylistUrl(feedUrl, apiKey);
  if (!endpoint) {
    return null;
  }
  const config = {
    schema: YOUTUBE_VIDEOS_SCHEMA,
    request: {
      endpoint,
      resourceType: 'json',
      headers: {
        'Content-Type': 'application/json',
      },
    },
  };
  return find(config, YOUTUBE_VIDEOS_SCHEMA);
}

function fetchUserUploads(feedUrl, apiKey, playlistIds) {
  const params = {
    type: 'playlistItems',
    playlistId: playlistIds,
    key: apiKey,
  };
  return find(YOUTUBE_VIDEOS_SCHEMA, undefined, { ...params, ...sharedParams });
}

function buildFeedParams(feedUrl, apiKey) {
  if (/channel/i.test(feedUrl)) {
    return {
      type: 'search',
      channelId: extractChannelId(feedUrl),
      key: apiKey,
    };
  } else if (/&list/i.test(feedUrl)) {
    return {
      type: 'playlistItems',
      playlistId: extractPlaylistId(feedUrl),
      key: apiKey,
    };
  }
  return {};
}

function userFeedThunk(feedUrl, apiKey) {
  let params;
  return (dispatch) => {
    dispatch(fetchUserPlaylistId(feedUrl, apiKey)).then((action) => {
      const { payload } = action;
      const playlistIds = _.map(payload.items, items =>
          (_.get(items, 'contentDetails.relatedPlaylists.uploads')),
        );
      dispatch(fetchUserUploads(feedUrl, apiKey, playlistIds));
    });
    return find(YOUTUBE_VIDEOS_SCHEMA, undefined, { ...params, ...sharedParams });
  };
}

export function loadFeed(feedUrl, apiKey) {
  if (/user/i.test(feedUrl)) {
    return userFeedThunk(feedUrl, apiKey);
  }

  const params = buildFeedParams(feedUrl, apiKey);
  return find(YOUTUBE_VIDEOS_SCHEMA, undefined, { ...params, ...sharedParams });
}
