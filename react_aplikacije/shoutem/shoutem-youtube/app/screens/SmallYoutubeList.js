import React from 'react';
import { connect } from 'react-redux';

import { connectStyle } from '@shoutem/theme';

import { ext } from '../const';

import { YoutubeList, mapStateToProps, mapDispatchToProps } from './YoutubeList';
import SmallYoutubeView from '../components/SmallYoutubeView';

class SmallYoutubeList extends YoutubeList {
  renderRow(video) {
    return (
      <SmallYoutubeView video={video} onPress={this.openDetailsScreen} />
    );
  }
}

export default connectStyle(ext('SmallYoutubeList'))(
  connect(mapStateToProps, mapDispatchToProps)(SmallYoutubeList),
);
