import React, {
  PropTypes,
} from 'react';
import moment from 'moment';
import _ from 'lodash';

import {
  ScrollView,
  Title,
  Video,
  Screen,
  Caption,
  Tile,
  View,
} from '@shoutem/ui';
import {
  RichMedia,
} from '@shoutem/ui-addons';
import { connectStyle } from '@shoutem/theme';
import { NavigationBar } from '@shoutem/ui/navigation';

import { ext } from '../const';

const propTypes = {
  video: PropTypes.object,
};


function YoutubeDetails({
  video,
}) {
  const playlistVideoSource = _.get(video, 'snippet.resourceId.videoId');
  const videoSource = _.get(video, 'id.videoId');
  const videoUrl = `https://youtube.com/watch?v=${playlistVideoSource || videoSource}`;
  const videoComponent = <Video source={{ uri: videoUrl }} />;
  const titleSource = _.get(video, 'snippet.title');
  const descriptionSource = _.get(video, 'snippet.description');
  const publishedAt = _.get(video, 'snippet.publishedAt');

  return (
    <Screen styleName="paper">
      <NavigationBar
        share={{
          title: titleSource,
          link: videoUrl,
        }}
        animationName="boxing"
        title={titleSource}
      />

      <ScrollView>
        {videoComponent}

        <Tile styleName="text-centric">
          <Title styleName="md-gutter-bottom">{titleSource}</Title>
          <Caption>{moment(publishedAt).fromNow()}</Caption>
        </Tile>

        <View styleName="solid">
          <RichMedia body={descriptionSource} />
        </View>
      </ScrollView>
    </Screen>
  );
}

YoutubeDetails.propTypes = propTypes;

export default connectStyle(ext('YoutubeDetails'))(YoutubeDetails);
