import { connect } from 'react-redux';
import React from 'react';
import _ from 'lodash';

import { ListView } from '@shoutem/ui';
import {
  next,
  isBusy,
  isInitialized,
} from '@shoutem/redux-io';
import { navigateTo } from '@shoutem/core/navigation';

import { getExtensionSettings } from 'shoutem.application';
import { RssListScreen } from 'shoutem.rss';

import { ext } from '../const';
import { YOUTUBE_VIDEOS_SCHEMA, getVideosFeed, loadFeed } from '../redux';
import LargeYoutubeView from '../components/LargeYoutubeView';

export class YoutubeList extends RssListScreen {
  static propTypes = {
    ...RssListScreen.propTypes,
    loadFeed: React.PropTypes.func,
    feed: React.PropTypes.object,
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      ...this.state,
      schema: YOUTUBE_VIDEOS_SCHEMA,
    };
    this.openDetailsScreen = this.openDetailsScreen.bind(this);
    this.renderRow = this.renderRow.bind(this);
    this.renderFeed = this.renderFeed.bind(this);
  }

  openDetailsScreen(video) {
    const { navigateTo, feedUrl } = this.props;
    const route = {
      screen: ext('YoutubeDetails'),
      props: {
        video,
        feedUrl,
      },
    };

    navigateTo(route);
  }

  fetchFeed() {
    const { feedUrl, apiKey } = this.props;

    if (_.isEmpty(feedUrl)) {
      return;
    }

    this.props.loadFeed(feedUrl, apiKey);
  }

  renderRow(video) {
    return (
      <LargeYoutubeView
        video={video}
        onPress={this.openDetailsScreen}
      />
    );
  }

  renderFeedData(feed) {
    return (
      <ListView
        data={_.get(feed, 'items')}
        renderRow={this.renderRow}
        loading={isBusy(feed) || !isInitialized(feed)}
        onRefresh={this.fetchFeed}
        onLoadMore={this.loadMore}
      />
    );
  }
}

export const mapStateToProps = (state, ownProps) => {
  const feedUrl = _.get(ownProps, 'shortcut.settings.feedUrl');
  const { apiKey } = getExtensionSettings(state, ext());

  return {
    feedUrl,
    apiKey,
    feed: getVideosFeed(state),
  };
};

export const mapDispatchToProps = { navigateTo, loadFeed, next };

export default connect(mapStateToProps, mapDispatchToProps)(YoutubeList);
