import React, { PropTypes } from 'react';
import { isBusy } from '@shoutem/redux-io';
import { getFormatDuration } from '../../services/duration';
import './style.scss';

export default function FeedPreviewTable(props) {
  const { feedItems } = props;
  const loading = isBusy(feedItems);

  return (
    <table className="table feed-preview-table">
      <thead>
        <tr>
          <th className="feed-preview-table__title">Title</th>
          <th className="feed-preview-table__duration">
            <span className="feed-preview-table__duration-margin">Duration</span></th>
        </tr>
      </thead>
      <tbody>
        {loading && (
          <tr>
            <td colSpan="2">loading content...</td>
          </tr>
        )}
        {!loading && (feedItems.length === 0) && (
          <tr>
            <td colSpan="2">No content to show</td>
          </tr>
        )}
        {!loading && feedItems.map(item => (
          <tr key={item.id}>
            <td className="feed-preview-table__title-overflow">
              <img className="feed-preview-table__img" src={item.thumbnails.default.url} />
              {item.title}
            </td>
            <td>
              <span className="feed-preview-table__duration-margin">
                {getFormatDuration(item.duration)}
              </span>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

FeedPreviewTable.propTypes = {
  feedItems: PropTypes.array,
};
