import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { isBusy, clear, shouldRefresh, isInitialized } from '@shoutem/redux-io';
import _ from 'lodash';
import {
  fetchShortcut,
  updateShortcutSettings,
  getShortcut,
  fetchExtension,
  getExtension,
} from '@shoutem/api';
import { LoaderContainer } from '@shoutem/react-web-ui';
import {
  FEED_ITEMS,
  loadFeed,
  getFeedItems,
} from '../../redux';
import FeedUrlInput from '../../components/feed-url-input';
import FeedPreview from '../../components/feed-preview';
import ext from '../../const';
import { validateYoutubeUrl } from '../../services/youtube';

const ACTIVE_SCREEN_INPUT = 0;
const ACTIVE_SCREEN_PREVIEW = 1;

export class YoutubeFeedPage extends Component {
  constructor(props) {
    super(props);

    this.getActiveScreen = this.getActiveScreen.bind(this);
    this.saveFeedUrl = this.saveFeedUrl.bind(this);
    this.handleFeedUrlInputContinueClick = this.handleFeedUrlInputContinueClick.bind(this);
    this.handleFeedPreviewRemoveClick = this.handleFeedPreviewRemoveClick.bind(this);

    props.fetchExtension();
    props.fetchShortcut();

    this.state = {
      error: null,
      hasChanges: false,
      feedUrl: _.get(props.shortcut, 'settings.feedUrl'),
      apiKey: _.get(props.extension, 'settings.apiKey'),
    };
  }

  componentWillReceiveProps(nextProps) {
    const { feedUrl } = this.state;
    const { extension } = this.props;
    const { extension: nextExtension } = nextProps;
    const { shortcut } = this.props;
    const { shortcut: nextShortcut } = nextProps;
    const { loadFeed } = nextProps;
    const { apiKey } = this.state;

    if (_.isEmpty(apiKey)) {
      this.setState({
        apiKey: _.get(nextExtension, 'settings.apiKey'),
      });
    }

    if (extension !== nextExtension &&
      shouldRefresh(nextExtension)) {
      this.props.fetchExtension();
    }

    if (_.isEmpty(feedUrl)) {
      const feedUrl = _.get(nextShortcut, 'settings.feedUrl');
      const apiKey = _.get(nextExtension, 'settings.apiKey');
      this.setState({
        feedUrl,
      });
      if (validateYoutubeUrl(feedUrl)) {
        loadFeed(feedUrl, apiKey).catch((err) => {
          this.setState({ error: _.get(err, 'message') });
        });
      }
    }

    if (
     shortcut !== nextShortcut &&
     shouldRefresh(nextShortcut)
   ) {
      this.props.fetchShortcut();
    }
  }

  getActiveScreen() {
    const { feedUrl } = this.state;
    if (!_.isEmpty(feedUrl)) {
      return ACTIVE_SCREEN_PREVIEW;
    }
    return ACTIVE_SCREEN_INPUT;
  }

  saveFeedUrl(feedUrl) {
    const { shortcut } = this.props;
    const settings = { feedUrl };

    this.setState({ error: '', inProgress: true });
    this.props.updateShortcutSettings(shortcut, settings).then(() => {
      this.setState({ hasChanges: false, inProgress: false });
    }).catch((err) => {
      this.setState({ error: err, inProgress: false });
    });
  }

  handleFeedUrlInputContinueClick(feedUrl) {
    const { apiKey } = this.state;
    this.setState({
      feedUrl,
    });
    this.saveFeedUrl(feedUrl);
    this.props.loadFeed(feedUrl, apiKey).catch((err) => {
      this.setState({ error: _.get(err, 'message') });
    });
  }

  handleFeedPreviewRemoveClick() {
    this.setState({
      feedUrl: null,
    });

    this.saveFeedUrl(null);
    this.props.clearFeedItems();
  }

  render() {
    const activeScreen = this.getActiveScreen();
    const { feedUrl } = this.state;
    const { feedItems, shortcut } = this.props;
    const initialized = isInitialized(shortcut);

    return (
      <LoaderContainer isLoading={!initialized} className="youtube-feed-page">
        {(activeScreen === ACTIVE_SCREEN_INPUT) && (
          <FeedUrlInput
            inProgress={isBusy(feedItems)}
            error={this.state.error}
            onContinueClick={this.handleFeedUrlInputContinueClick}
          />
        )}
        {(activeScreen === ACTIVE_SCREEN_PREVIEW) && (
          <div className="youtube-feed-page__content">
            <FeedPreview
              feedUrl={feedUrl}
              feedItems={feedItems}
              onRemoveClick={this.handleFeedPreviewRemoveClick}
            />
          </div>
        )}
      </LoaderContainer>
    );
  }
}

YoutubeFeedPage.propTypes = {
  shortcut: PropTypes.object,
  extension: PropTypes.object,
  feedItems: PropTypes.array,
  fetchExtension: PropTypes.func,
  fetchShortcut: PropTypes.func,
  updateShortcutSettings: PropTypes.func,
  loadFeed: PropTypes.func,
  clearFeedItems: PropTypes.func,
};


function mapStateToProps(state, ownProps) {
  const { shortcutId } = ownProps;
  const extensionName = ext();

  return {
    extension: getExtension(state, extensionName),
    shortcut: getShortcut(state, shortcutId),
    feedItems: getFeedItems(state, extensionName, shortcutId),
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  const { shortcutId } = ownProps;

  return {
    fetchExtension: () => dispatch(fetchExtension(ext())),
    fetchShortcut: () => dispatch(fetchShortcut(shortcutId)),
    updateShortcutSettings: (shortcut, { feedUrl }) => (
      dispatch(updateShortcutSettings(shortcut, { feedUrl }))),
    clearFeedItems: () => dispatch(clear(FEED_ITEMS, 'feedItems')),
    loadFeed: (url, apiKey) => dispatch(loadFeed(url, apiKey, shortcutId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(YoutubeFeedPage);
