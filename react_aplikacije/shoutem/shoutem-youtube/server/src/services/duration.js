import moment from 'moment';
import 'moment-duration-format';

export function getFormatDuration(duration) {
  return moment.duration(duration).format('H[:]m:ss', { trim: 'left', forceLength: true });
}
