import validator from 'validator';

// channelId is going to be extracted if matches the regex function
const channelRegex = /(?:http|https:\/\/|)www.youtube\.com\/channel\/([a-zA-Z0-9]{1,}_[a-zA-Z0-9]{1,})/i;

const userRegex = /(?:http|https:\/\/|)www.youtube\.com\/user\/([a-zA-Z0-9]{1,})/i;

const playlistRegex = /^.*(?:youtu.be\/|list=)([^#]*).*/i;

export function isChannelUrl(feedUrl) {
  return channelRegex.test(feedUrl);
}

export function isUserUrl(feedUrl) {
  return userRegex.test(feedUrl);
}

export function isPlaylistUrl(feedUrl) {
  return playlistRegex.test(feedUrl);
}

export function resolveVideosSearchEndpoint(feedUrl, apiKey) {
  if (isChannelUrl(feedUrl)) {
    const channelId = feedUrl.match(channelRegex)[1];
    return `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&channelId=${channelId}&key=${apiKey}`;
  } else if (isPlaylistUrl(feedUrl)) {
    const playlistId = feedUrl.match(playlistRegex)[1];
    return `https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=20&playlistId=${playlistId}&key=${apiKey}`;
  }
  return null;
}


export function resolveUserChannel(feedUrl, apiKey) {
  if (isUserUrl(feedUrl)) {
    const userId = feedUrl.match(userRegex)[1];
    return `https://www.googleapis.com/youtube/v3/channels?part=snippet,contentDetails&forUsername=${userId}&key=${apiKey}`;
  }
  return null;
}

export function resolveVideosFetchEndpoint(feedUrl, apiKey, videoIds) {
  const videoIdsStr = videoIds.join(',');
  if (isChannelUrl(feedUrl)) {
    const channelId = feedUrl.match(channelRegex)[1];
    return `https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&id=${videoIdsStr}&maxResults=20&channelId=${channelId}&key=${apiKey}`;
  } else if (isPlaylistUrl(feedUrl)) {
    const playlistId = feedUrl.match(playlistRegex)[1];
    return `https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&id=${videoIdsStr}&maxResults=20&playlistId=${playlistId}&key=${apiKey}`;
  }
  return null;
}

export function createYoutubeValidationUrl(apiKey) {
  return `https://www.googleapis.com/youtube/v3/channels?part=snippet&id=UCK8sQmJBp8GCxrOtXWBpyEA&key=${apiKey}`;
}

export function createYoutubePlaylistUrl(playlistId) {
  return `https://www.youtube.com/playlist?list=${playlistId}`;
}

const validateUrl = url => validator.isURL(url, { require_protocol: false });

export function validateYoutubeUrl(url) {
  if (!url) {
    return false;
  }
  if (!validateUrl(url)) {
    return false;
  } else if (!isChannelUrl(url) && !isUserUrl(url) && !isPlaylistUrl(url)) {
    return false;
  }
  return true;
}
