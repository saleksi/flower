SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `cvijece` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` text NOT NULL,
  `slika` longtext NOT NULL,
  `period_ljeto` int(11) NOT NULL,
  `period_zima` int(11) NOT NULL,
  `datum_zaliven` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `korisnici` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` VARCHAR(32) NOT NULL,
  `pass` VARCHAR(32) NOT NULL,
  `email` text NOT NULL,
  `Ime` text NOT NULL,
  `Prezime` text NOT NULL,
  PRIMARY KEY (`id`),
	UNIQUE KEY `key1` (`user`,`pass`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;

CREATE TABLE IF NOT EXISTS `zalijevanje` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_korisnika` int(11) NOT NULL,
  `id_cvijeta` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key2` (`id_korisnika`,`id_cvijeta`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0;

DELIMITER //

DROP PROCEDURE IF EXISTS `DohvatiCvijeceSortirano` //
CREATE PROCEDURE `DohvatiCvijeceSortirano` ( IN `userId` INT(11), IN `orderByClause` VARCHAR(32) )
BEGIN
DECLARE s VARCHAR(1024);
SET @s = CONCAT('SELECT cvijece.id, cvijece.ime, cvijece.slika, cvijece.period_ljeto, cvijece.period_zima, cvijece.datum_zaliven FROM cvijece JOIN zalijevanje ON zalijevanje.id_cvijeta = cvijece.id AND zalijevanje.id_korisnika = ', userId ,' ORDER BY ', orderByClause, ' ;');
PREPARE e from @s;
EXECUTE e;
END
//

DROP PROCEDURE IF EXISTS `DohvatiCvijece` //
CREATE PROCEDURE `DohvatiCvijece`( IN `user_id` INT(11) )
BEGIN
SELECT `cvijece`.`id`,`cvijece`.`ime`,`cvijece`.`slika`,`cvijece`.`period_ljeto`,`cvijece`.`period_zima`,`cvijece`.`datum_zaliven` FROM `cvijece` JOIN `zalijevanje` ON `zalijevanje`.`id_cvijeta`=`cvijece`.`id` AND `zalijevanje`.`id_korisnika`=`user_id`;
END
//

DROP PROCEDURE IF EXISTS `BrisanjeKorisnika` //
CREATE PROCEDURE `BrisanjeKorisnika` ( IN `user_id` INT(11) )
BEGIN
DELETE FROM `korisnici` WHERE `korisnici`.`id`=`user_id`;
DELETE FROM `zalijevanje` WHERE `zalijevanje`.`id_korisnika`=`user_id`;
COMMIT;
END
//

DROP PROCEDURE IF EXISTS `UnosCvijeta` //
CREATE PROCEDURE `UnosCvijeta`( IN `i` TEXT, IN `s` LONGTEXT, IN `p1` INT(11), IN `p2` INT(11), IN `d` TEXT, IN `user_id` INT(11) )
BEGIN
INSERT INTO `cvijece`(ime,slika,period_ljeto,period_zima,datum_zaliven) VALUES (`i`,`s`,`p1`,`p2`,`d`);
SELECT LAST_INSERT_ID() INTO @cvijet_id;
INSERT INTO zalijevanje VALUES (NULL,`user_id`,@cvijet_id);
COMMIT;
END
//

DROP PROCEDURE IF EXISTS `BrisanjeCvijeta` //
CREATE PROCEDURE `BrisanjeCvijeta`(IN `flower_id` INT(11))
BEGIN
DELETE FROM `cvijece` WHERE `cvijece`.`id` = `flower_id`;
DELETE FROM `zalijevanje` WHERE `zalijevanje`.`id_cvijeta` = `flower_id`;
COMMIT;
END
