<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <link href="images/favicon.ico" rel="shortcut icon">
  <link rel="stylesheet" href="css/style.css">
  <link rel="icon" type="image/png" sizes="96x96" href="/images/favicon-96x96.png">
  <title>Cvijeće</title>
  <?php if($bLoggedIn==true){echo "
		<script src='js/jquery-2.0.0.min.js'></script>
		<!-- notify -->
		<script src='js/notify.min.js'></script>
		<!-- glyphicons -->
		<link rel='stylesheet' href='css/modified.bootstrap.css'>
		<style type='text/css'>
			@font-face{font-family: 'Glyphicons Halflings';
			src: url('../fonts/glyphicons-halflings-regular.eot');
			src: url('../fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'),
			url('../fonts/glyphicons-halflings-regular.woff') format('woff'),
			url('../fonts/glyphicons-halflings-regular.ttf') format('truetype'),
			url('../fonts/glyphicons-halflings-regular.svg#glyphicons-halflingsregular') format('svg'); }
		</style>
		<!-- cropit -->
		<link rel='stylesheet' href='css/cropit.css'></link>
		<script src='js/jquery.cropit.js'></script>
		<!-- datepicker -->
		<script src='js/jquery.plugin.js'></script>
		<script src='js/jquery.datepick.js'></script>
		<script src='js/jquery.datepick-hr.js'></script>
		<script>\$(function(){
			\$(\".datepicker\").datepick({
				dateFormat:'d.m.yyyy.',
				onSelect:UnosDatuma});
		});</script>
		<link rel='stylesheet' href='css/flora.datepick.css'>
		<!-- barcode -->
		<script src='js/jquery.barcode.js'></script>
		<!-- sleep function for delays -->
		<script>function sleep(milliseconds) {
			var start = new Date().getTime();
			for (var i = 0; i < 1e7; i++){
				if ((new Date().getTime() - start) > milliseconds){
					break;
				}
			}
		}</script>";
		}?>
</head>
<body>
  <div class="container">
    <header class="header clearfix">
      <div class="logo">Zalijevanje cvijeća</div>
      <nav class="menu_main">
        <ul>
          <li <?php echo $active[0];?>><a href="index.php">Početna</a></li>
          <?php if($bLoggedIn==true){ // samo ulogirani korisnici vide cijeli menu
					?>
          <li <?php echo $active[1];?>><a href="zalijevanje.php">Zalijevanje</a></li>
					<li <?php echo $active[2];?>><a href="unos.php">Unos</a></li>
					<?php }?>
        </ul>
      </nav>
    </header>
    <div class="info">
      <article class="hero clearfix">
        <div class="col_100">
          <h1 id="LocalTitle">...</h1>
          <p id="LocalSubTitle">...</p>
        </div>
      </article>
      <article class="article clearfix">
