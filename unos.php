<?php

	include 'database.php'; // spajanje s bazom

	include "main.php"; // sesija, login, submitt form...

	if($bLoggedIn==false){
		header("location:index.php");
		exit();
	}

	$active = array("", "", "class='active'"); // odabir akritnog polja u meniju

	include "top.php"; // html head, main manu, scripts...

?>

<script>
	document.getElementById('LocalTitle').innerHTML = "Unos novog cvijeta";
	document.getElementById('LocalSubTitle').innerHTML =
		"Unesite sliku, podatke o zalijevanju i ime novog cvijeta ili upišite postojeći cvijet barkod čitačem u sijedećem polju: " +
		"<span style='display:block;float:right;'>" +
		"<input type='text' id='link_barcode_text' placeholder='Barkod oznaka' style='display: inline-block; opaque: 0.5;'  onkeypress='handle(event)' >" +
		"</span>";
</script>
		<br>
		<br>
		<div class ="col_33">
			<legend class ="button2">
				<p><br></p>
				<h2>Slika cvijeta</h2>
				<p><br></p>
			</legend>
			<div class="image-editor">
				<div style="display: table;">
					<div style="display: table-row; width: 100%;">
						<span id="zoom">
							<span style="display: table-cell; padding: 3px; font-size: 1.2em;" class="glyphicon glyphicon-picture"></span>
							<span style="display: table-cell; padding: 3px;"><input type="range" class="cropit-image-zoom-input"></span>
							<span style="display: table-cell; padding: 3px; font-size: 2.2em;" class="glyphicon glyphicon-picture"></span>
						</span>
						<span style="display: table-cell; padding: 5px;"></span>
						<span id="rL" style="display: table-cell; padding: 3px; font-size: 1.8em;" class="rotate-ccw glyphicon glyphicon-repeat mirror"></span>
						<span id="rR" style="display: table-cell; padding: 3px; font-size: 1.8em;" class="rotate-cw glyphicon glyphicon-repeat"></span>
						<span style="display: table-cell; padding: 5px;"></span>
						<span id="upload" style="display: table-cell; padding: 3px; font-size: 1.8em;" class="cropit-image-input-btn  glyphicon glyphicon-upload"></span>
					</div>
				</div>
				<div class="cropit-preview"></div>
				<input type="file" class="cropit-image-input" hidden>
			</div>
		</div>
		<div class ="col_33">
			<legend class ="button2">
				<p><br></p>
				<h2>Podaci o zalijevanju</h2>
				<p><br></p>
			</legend>

			<p><label>Datum prethodnog zalijevanja:</label>
			<input class="datepicker" id="datepicker" type="text" required readonly onchange="UnosDatuma();"></p>

			<p><label for="p1">Ljetni period zalijevanja (dana):</label>
			<input type="number" name="p1" id="p1" min="1" max="60" value="7" required></p>

			<p><label for="p2">Zimski period zalijevanja (dana):</label>
			<input type="number" name="p2" id="p2" min="1" max="60" value="14" required></p>


		</div>
		<div class ="col_33">

			<legend class ="button2">
				<p><br></p>
				<h2>Spremanje podataka</h2>
				<p><br></p>
			</legend>

			<form id="myForm" method="post" action="unos.php">
				<p><label>Ime cvijeta:</label></p>
				<input type="text" name="ime" value="Cvijet" id="ime">
				<input type="hidden" name="slika"            id="slika">
				<input type="hidden" name="period_ljeto"     id="period_ljeto">
				<input type="hidden" name="period_zima"      id="period_zima">
				<input type="hidden" name="datum_zaliven"    id="datum_zaliven">
				<input type="submit" name="novi_cvijet"      id="novi_cvijet" style="visibility:hidden;">
			</form>

			<p><button class='button' id="validateForm">Spremi podatke o cvijetu</button></p>

			<script>
				function UnosDatuma(){
					document.getElementById('datum_zaliven').value = $(".datepicker").datepick().val();
				};

				$('.image-editor').cropit({imageState:{src:'images/cvijet.png',},});

				$('.cropit-image-input-btn').click(function(){
					$('.cropit-image-input').click();
				});

				$('.rotate-cw').click(function(){
					$('.image-editor').cropit('rotateCW');
				});

				$('.rotate-ccw').click(function(){
					$('.image-editor').cropit('rotateCCW');
				});

				$(document).on("load",".image-editor",function(){
					document.getElementById('slika').value = $('.image-editor').cropit('export');
				});

				$( document ).ready(function() {
					document.getElementById('datepicker').value = $.datepick.formatDate($.datepick.determineDate("Today (0)"));
				});

				$(document).on("click","#validateForm",function(){
					document.getElementById('slika').value = $('.image-editor').cropit('export');

					sleep(3000); // delay 3 s treba za velike slike jer se relativno dugo uploadaju

					document.getElementById('period_ljeto').value = document.getElementById('p1').value;
					document.getElementById('period_zima').value = document.getElementById('p2').value;
					document.getElementById('datum_zaliven').value = $("#datepicker").datepick({ dateFormat: 'yy-mm-dd' }).val();

					var A = ['ime','slika','period_ljeto','period_zima','datum_zaliven'];
					var i;
					for( i in A){
						var x = document.getElementById(A[i]).value;
						if(x == null || x == ""){
							alert('Ispunite sva polja forme.');
							return false;
						}
					}

					document.getElementById('novi_cvijet').click();
				});

				function LinkBarcode(){
					var ean13text = document.getElementById('link_barcode_text').value;

					// convert 'ean13text' to 'flower_id'
					flower_id = Number(ean13text.substring(0, ean13text.length - 1));// ukloni zadnji znak
					document.getElementById('link_barcode_id').value = flower_id;

					document.getElementById('link_barcode_submit').click();
				}

				function handle(e){
					if(e.keyCode === 13){LinkBarcode();} // enter tipha = 13 ASCII
					else{return false};
				}

				<?php // uporaba notify.js skripte
					if($_SESSION['CvijetDodan']){
						echo "\$.notify('Cvijet je uspješno dodan u bazu podataka',{className:'success',position:'left top'});";
					}
					if($_SESSION['PovezanCvijetKorisnik']){
						echo "\$.notify('Cvijet je dodan na popis vašeg cvijeća',{className:'success',position:'left top'});";
					}
					if($_SESSION['NijePovezanCvijetKorisnik']){
						echo "\$.notify('Od prije ste povezani s ovim cvijetom.',{className:'warn',position:'left top'});";
					}
					if($_SESSION['CvijetNePostoji']){
						echo "\$.notify('Ne postoji cvijet s ovim barkodom.',{className:'error',position:'left top'});";
					}
				?>

			</script>
		</div>

		<div style="display:none;">
			<form name='LinkBarcodeForm' method='post' action='unos.php'>
				<input type='text'   name='link_barcode_id'     id='link_barcode_id'     style='visibility:hidden;'>
				<input type='submit' name='link_barcode_submit' id='link_barcode_submit' style='visibility:hidden;'>
			</form>
		</div>

<!-- Footer -->
<?php include "bottom.php"; ?>
