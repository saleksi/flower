<?php

	include "database.php"; // spajanje s bazom

	include "main.php"; // sesija, login, submitt form...

	if($bLoggedIn==false){
		header("location:index.php");
		exit();
	}

	$active = array("", "class='active'", ""); // odabir akritnog polja u meniju

	include "top.php"; // html head, main manu, scripts...

?>

<script type="text/javascript" src="js/jquery-3.0.0.min.js"></script>
<script type="text/javascript" src="js/jquery-barcode.js"></script>

<!-- ZALIJEVANJE -->
<script>

	document.getElementById('LocalTitle').innerHTML = "Zalijevanje cvijeća";
	document.getElementById('LocalSubTitle').innerHTML = "" +
	 "Klikom na datum postavite kada je Vaš cvijet zaliven. Trenutno zalijevanje unesite klikom na " +
	 "<span style='font-size: 0.9em; padding: 2px;' class='glyphicon glyphicon-tint'></span> " +
	 "ili barkod čitačem u slijedeće polje: " +
	 "<span style='display:block;float:right;'>" +
   "<input type='text' name='barcode_text' id='barcode_text' placeholder='Barkod oznaka' style='display: inline-block; opaque: 0.5;'  onkeypress='handle(event)' >"+
   "" +
	 "</span>";

	function BarCodeStr(id){$('#'+id).barcode(id,'ean13',{barWidth:2,barHeight:20})};

</script>

<div class="clearfix" ></div>
	<br>
	<div style="display:inline;">
		Sortiraj cvijeće prema:
		<form id="SortFlowerForm1" name="SortFlowerForm1" method="post" action="zalijevanje.php" style="display:inline;">
			<select id="sortSelect1" name="sortSelect1" form="SortFlowerForm1" onChange="this.form.submit()">;
				<?php
					for($i=0; $i<3; $i++) {
						echo "<option value=".$i." ";
						if($i == $order1) { echo "selected"; }
						echo ">".$orderByValueArray[$i]."</option>";
					}
				?>
			</select>
		</form>
		<form id="SortFlowerForm2" name="SortFlowerForm2" method="post" action="zalijevanje.php" style='display:inline;'>
			<?php
				$hide = ($order1 == 0) ? "hidden" : " style='display:inline;'";
				echo "<select id='sortSelect2' name='sortSelect2' form='SortFlowerForm2' onChange='this.form.submit()' ".$hide.">";
				if($order1 > 0) {
					for($i=0; $i<3; $i++) {
						if($i == $order1) { continue; }
						echo "<option value=".$i." ";
						if($i == $order2) { echo "selected"; }
						echo ">".$orderByValueArray[$i]."</option>";
					}
				}
				echo "</select>";
			?>
		</form>
	</div>
	<br>

<div class="clearfix"></div>
<?php
	// dohvati cvijeće iz baze za korisnika
	$q="CALL DohvatiCvijeceSortirano(".$_SESSION['id'].",\"".$orderString."\");";
	$res = mysqli_query($con, $q);
	$tot = mysqli_num_rows($res);

	$cnt=0;
	$datepickers = "";
	while($tot--)
	{
		$row = mysqli_fetch_row($res);
		$cnt++;

		$id=$row[0];
		$ime=$row[1];
		$slika=$row[2];
		$p1=$row[3];
		$p2=$row[4];
		$d1=$row[5];

		// barcode string
		$id_str = (string)$id;
		while(strlen($id_str)<12){
			$id_str = '0' . $id_str;
		}

		// razlika datuma u danima
		list($d,$m,$y)=explode('.',$d1);
		$zaliven = mktime(0, 0, 0,      $m  ,      $d  ,      $y  );
		$danas   = mktime(0, 0, 0, date("m"), date("d"), date("Y"));

		$d = floor(($danas-$zaliven)/86400);

		// upozorenje zalijevanja
		$myClass = "button3"; // crveno
		if($d>=0){
			$month = date("m");
			// ljetni(4,5,6,7,8,9) : zimski(10,11,12,1,2,3)
			$d = ($month>3 && $month<10) ? $d-$p1 : $d-$p2;
			if($d<0){
				$myClass = "button2"; // zeleno
			}
		}

		echo "<div class='col_25 center' id='A".$id."'>". // anchor ili bookmark
					 "<legend class ='$myClass'><p><br></p><h2>$ime</h2><p><br></p></legend>
						<div class='ib'><p><img src='$slika'><br><span id='$id_str' style='padding: 0px;'></span>
							<br><span id='delete_flower' onclick=\"DeleteFlower(".$id.");\"
							 style='font-size: 0.9em; padding: 5px;' class='glyphicon glyphicon-trash'></span>
							 <input id=\"datepicker_".$id."\" class='datepicker'  value='$d1' style='display:inline-block; border:0px; width:80px; text-align: center;' onblur='SaveID($id)'/> $p1/$p2
							 <span id='plant_flower' onclick=\"PlantFlower(".$id.");\"
							 style='font-size: 0.9em; padding: 5px;' class='glyphicon glyphicon-tint'></span>
						</div>
						<script>BarCodeStr(\"". $id_str ."\");</script>
					</div>";

		if($cnt==4){
			$cnt = 0;
			if($tot){
				echo "<div class='clearfix'></div>";
			}
		}
	}
?>

<script>
	function SortSelect(selectId){

	}

	function PlantFlower(flower_id){
		var d = $.datepick.formatDate($.datepick.determineDate("Today (0)"));
		document.getElementById('plant_date').value = d;
		document.getElementById('plant_id').value = flower_id;
		document.getElementById('plant_submit').click();
	}

	function DeleteFlower(flower_id){
		document.getElementById('delete_id').value = flower_id;
		document.getElementById('delete_submit').click();
	}

	function BarcodeFlower(){
		var ean13text = document.getElementById('barcode_text').value;

		// convert 'ean13text' to 'flower_id'
		flower_id = Number(ean13text.substring(0, ean13text.length - 1));// ukloni zadnji znak
		document.getElementById('barcode_id').value = flower_id;

		var d = $.datepick.formatDate($.datepick.determineDate("Today (0)"));
		document.getElementById('barcode_date').value = d;

		document.getElementById('barcode_submit').click();
	}

	function handle(e){
		if(e.keyCode === 13){BarcodeFlower();} // enter tipha = 13 ASCII
		else{return false};
	}

	var dp_id; // last used datepicker's id
	function SaveID(id)
	{
		dp_id = id;
	}

	function UnosDatuma(){
		document.getElementById('plant_edit_date').value = document.getElementById("datepicker_"+dp_id.toString()).value;
		document.getElementById('plant_edit_id').value = dp_id.toString();
		document.getElementById('plant_edit_submit').click();
	};

	<?php // uporaba notify.js skripte
		if($_SESSION['CvijetObrisan']){
			echo "\$.notify('Cvijet obrisan',{className:'success',position:'top left'});";
		}
		if($_SESSION['CvijetZaliven']){
			echo "\$.notify('Cvijet zaliven',{className:'success',position:'top left'});";
		}
		if($_SESSION['CvijetZalivenBarkodom']){
			echo "\$.notify('Cvijet zaliven',{className:'success',position:'top left'});";
		}
		if($_SESSION['IzmjenaDatumZaliven']){
			echo "\$.notify('Upisan datum zalijevanja',{className:'success',position:'top left'});";
		}
		if($_SESSION['Sortirano']){
			echo "\$.notify('Cvijeće je sortirano',{className:'success',position:'top left'});";
		}
	?>
</script>

<div style="display:none;">
	<form name="DeleteFlowerForm" method="post" action="zalijevanje.php">
		<input type="text"   name="delete_id"     id="delete_id"     style="visibility:hidden;">
		<input type="submit" name="delete_submit" id="delete_submit" style="visibility:hidden;"
					 onclick="return confirm('Sigurno želite obrisati cvijet?')">
	</form>
	<form name="PlantFlowerForm" method="post" action="zalijevanje.php">
		<input type="text"   name="plant_id"     id="plant_id"     style="visibility:hidden;">
		<input type="text"   name="plant_date"   id="plant_date"   style="visibility:hidden;">
		<input type="submit" name="plant_submit" id="plant_submit" style="visibility:hidden;">
	</form>
	<form name='PlantBarcodeForm' method='post' action='zalijevanje.php'>
		<input type="text"   name="barcode_id"     id="barcode_id"     style="visibility:hidden;">
		<input type="text"   name="barcode_date"   id="barcode_date"   style="visibility:hidden;">
		<input type='submit' name='barcode_submit' id='barcode_submit' style='visibility:hidden;'>
	</form>
	<form name="PlantEditDateForm" method="post" action="zalijevanje.php">
		<input type="text"   name="plant_edit_id"     id="plant_edit_id"     style="visibility:hidden;">
		<input type="text"   name="plant_edit_date"   id="plant_edit_date"   style="visibility:hidden;">
		<input type="submit" name="plant_edit_submit" id="plant_edit_submit" style="visibility:hidden;">
	</form>
</div>

<!-- Footer -->
<?php include "bottom.php"; ?>
